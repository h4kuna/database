# Database #

This database layer is primary for postgresql, but is expandable for other databases, not implemented, yet.

First time prepare connection and builder class.
```
#!php
<?php
use h4kuna\Database\Storage;

$connection = new Storage\Driver\PostgreSql\Connection($user, $password, $dbname);
$postgre = new Storage\PostgreSql($connection, TRUE); // TRUE is development enable
```

Idea is easy, first select table via [TableCollection](src/SqlBuilder/TableCollection.php?at=master&fileviewer=file-view-default) class. Where are methods for create while register to collection via **createTable()**. Fast get table via table name or table alias is parameter for **get()**.
```
#!php
<?php
$tableCollection = $postgre->getTableCollection();

/* @var $table \h4kuna\Database\SqlBuilder\Table */
$table = $tableCollection->createTable('users', 'u');
```

Object [Table](src/SqlBuilder/Table.php?at=master&fileviewer=file-view-default) has methods for create commands like SELECT, UPDATE, INSERT and DELETE (SUID). Fluent is fully supported. If you need sanitize any value add to statement via question mark (?) and add these values like parameters.

[Select class](src/SqlBuilder/Command/Select.php?at=master&fileviewer=file-view-default)
```
#!php
<?php
$user = $tableCollection->get('u');

/* @var $sqlUser \h4kuna\Database\SqlBuilder\Command\Select */
$sqlUser = $user->select('id, name');
foreach($sqlUser as $userRow) {
    dump($userRow);
}
```
If you don't execute command via iterator, you can use any methods in [SUIDAbstract](src/SqlBuilder/SUIDAbstract.php?at=master&fileviewer=file-view-default) fetch, fetchAll, fetchSingle, execute... Row is represented like class [Row](src/Result/Row.php?at=master&fileviewer=file-view-default)

Update/insert and returning is fully supported.
```
#!php
<?php
$user = $tableCollection->get('u');

/* @var $sqlUser \h4kuna\Database\SqlBuilder\Command\Insert */
$sqlUser = $user->insert(['name' => 'Doe'])->returning();
foreach($sqlUser as $userRow) {
    dump($userRow);
}

dump($user->select('id')->where('name', 'Doe')->fetch());
```
Using conndition is easy

* ->where('name', 'Doe') // name = 'Doe'
* ->where('name = ?', 'Doe') // name = 'Doe'
* ->where('!name', 'Doe') // name != 'Doe'
* ->where('name != ?', 'Doe') // name != 'Doe'
* ->where('name', ['Doe', 'Joe']) // name IN ('Doe', 'Joe')
* ->where('!name', ['Doe', 'Joe']) // name NOT IN ('Doe', 'Joe')
* ->where('name IN ?', ['Doe', 'Joe']) // name IN ('Doe', 'Joe')
* ->where('name', NULL) // name IS NULL
* ->where('!name', NULL) // name IS NOT NULL

Add next condition 
```
#!php
<?php
dump($user->select('id')->where('name', 'Doe')->andIf('password', '***')->orIf('email', 'doe@joe.com')->limit(1)->fetch());
// SELECT id FROM user WHERE name = 'Doe' AND password = '***' OR email = 'doe@joe.com' LIMIT 1;
```

## Support conversion type ##
There is basic [Grammar](src/Storage/Driver/PostgreSql/Grammar.php?at=master&fileviewer=file-view-default) whose care about data type like int, string, php DateTime to postgre 
and vice versa, simple array and boolean values.

## SubSelect ##
It is no problem create SELECT <subselect>, FROM <subselect>, WHERE <subselect>.