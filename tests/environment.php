<?php

/* @var $builder \h4kuna\Database\Test\SqlBuilder */
$builder = require __DIR__ . '/bootstrap.php';

// PostgreSql
$postgre = $builder->getPostgreSql();
$query = $postgre->getQuery();

$tables = [
	'CREATE TABLE IF NOT EXISTS author (
  id serial NOT NULL PRIMARY KEY,
  name character varying(50) NOT NULL,
  "unique" character varying(50) NULL UNIQUE);'
	,
	'CREATE TABLE IF NOT EXISTS book (
  id serial NOT NULL PRIMARY KEY,
  name character varying NOT NULL);'
	,
	'CREATE TABLE IF NOT EXISTS author_x_book (
  author_id integer NOT NULL REFERENCES author (id),
  book_id integer NOT NULL REFERENCES book (id),
  UNIQUE(author_id, book_id));'
];

foreach ($tables as $tableQuery) {
	$query->noExplainExecute($tableQuery);
}

