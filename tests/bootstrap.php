<?php

require __DIR__ . '/../vendor/autoload.php';

require __DIR__ . '/libs/SqlBuilder.php';
require __DIR__ . '/libs/GrammerNoSql.php';

date_default_timezone_set('Europe/Prague');
$tempDir = __DIR__ . '/temp';

function d(/* ... */)
{
	return call_user_func_array('dump', func_get_args());
}

/**
 * Dump and die.
 */
function dd(/* ... */)
{
	if (!Tracy\Debugger::$productionMode) {
		call_user_func_array('dump', func_get_args());
		exit;
	}
}

Tester\Environment::setup();
Tracy\Debugger::enable(FALSE, $tempDir);

$builder = new \h4kuna\Database\Test\SqlBuilder($tempDir);
return $builder;
