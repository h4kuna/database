<?php

namespace h4kuna\Database\Test;

use h4kuna\Database,
	h4kuna\Database\Storage;

class SqlBuilder
{

	private $tempDir;

	public function __construct($tempDir)
	{
		$this->tempDir = $tempDir;
	}

	/** @var Storage\PostgreSql */
	private $postgreSql;

	/** @return Storage\PostgreSql */
	public function getPostgreSql($trace = FALSE)
	{
		if ($this->postgreSql === NULL) {
			$this->postgreSql = $this->createPostgreSql($trace);
		}
		return $this->postgreSql;
	}

	/** @return Storage\PostgreSql */
	private function createPostgreSql($trace)
	{
		$params = $this->loadConnectionParameters('postgreSql');
		$connection = new Storage\Driver\PostgreSql\Connection($params['user'], $params['password'], $params['dbname']);
		$connection->setSetup(array_diff_key($params, ['user' => 1, 'password' => 1, 'dbname' => 1]));
		$postgre = new Storage\PostgreSql($connection, TRUE);

		if ($trace) {
			$traceFile = $this->tempDir . '/pgsSql';
			touch($traceFile);
			$postgre->getConnection()->trace($traceFile);
		}
		return $postgre;
	}

	private function loadConnectionParameters($db)
	{
		$file = __DIR__ . '/../config/db/' . $db . '.ini';
		if (!is_file($file)) {
			throw new Database\InvalidArgumentException('Ini file not found: ' . $file);
		}
		return parse_ini_file($file, TRUE);
	}

}
