<?php

namespace h4kuna\Database\Test;

use h4kuna\Database\Storage\Driver;

class GrammerNoSql extends Driver\GrammerAbstract
{

	public function markParameter($parameter)
	{

	}

	public function queryColumn($column)
	{
		return "[$column]";
	}

	public function queryTable(\h4kuna\Database\SqlBuilder\Table $table, $showAlias = TRUE)
	{

	}

	public function queryValue($value, $save = FALSE)
	{
		return '"' . $value . '"';
	}

	public function normalizeType($columnType)
	{

	}

	public function normalizeValue($value, $type)
	{

	}

}
