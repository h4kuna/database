<?php

namespace h4kuna\Database\SqlBuilder\Command;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../bootstrap.php';

class InsertTest extends \Tester\TestCase
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	public function __construct(SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
	}

	public function testInsertSelect()
	{
		$orders = $this->tableCollection->createTable('orders', 'o');
		$users = $this->tableCollection->createTable('users', 'u');
		$selectUser = $users->select('id, firstname')->where('id', 5);
		Assert::equal('INSERT INTO orders (user_id, name) VALUES (SELECT id, firstname FROM users AS u WHERE id = 5);', $orders->insert(['user_id, name' => $selectUser])->sql());
	}

	public function testInsert()
	{
		$table = $this->tableCollection->createTable('users', 'u');
		$insert = $table->insert(['name' => 'Milan', 'nick' => 'h4kuna']);
		Assert::equal("INSERT INTO users (name, nick) VALUES ('Milan', 'h4kuna');", $insert->sql());

		$insert = $table->insert(new \ArrayIterator(['name' => 'Milan', 'nick' => 'h4kuna', 'settings' => [1, 2, 3]]));
		Assert::equal("INSERT INTO users (name, nick, settings) VALUES ('Milan', 'h4kuna', '{1, 2, 3}');", $insert->sql());
	}

	public function testMultiInsert()
	{
		$table = $this->tableCollection->createTable('users');
		$insert = $table->insert(['name' => 'Milan', 'nick' => 'h4kuna']);
		$insert->addRow(new \ArrayIterator(['name' => 'Don', 'nick' => 'Joe']));

		Assert::equal("INSERT INTO users (name, nick) VALUES ('Milan', 'h4kuna'), ('Don', 'Joe');", $insert->sql());
	}

	public function testBindQuery()
	{
		$table = $this->tableCollection->createTable('users');
		$insert = $table->insert(['name' => 'Milan', 'nick' => 'h4kuna', 'surname' => 'foo', 'id' => 1]);
		$insert->addRow(new \ArrayIterator(['name' => 'Don', 'nick' => 'Joe', 'surname' => 'bar', 'id' => 2]))
			->addRow(['name' => 'Mark', 'nick' => 'Zu', 'surname' => 'Berg', 'id' => 3]);

		Assert::equal("INSERT INTO users (name, nick, surname, id) VALUES ('Milan', 'h4kuna', 'foo', '1'), ('Don', 'Joe', 'bar', '2'), ('Mark', 'Zu', 'Berg', '3');", $insert->getQuery()->sql());
	}

}

(new InsertTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
