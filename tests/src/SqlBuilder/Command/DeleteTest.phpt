<?php

namespace h4kuna\Database\SqlBuilder\Command;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../bootstrap.php';

class DeleteTest extends \Tester\TestCase
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	public function __construct(SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
	}

	/**
	 * @throws \h4kuna\Database\DangerousQueryException
	 */
	public function testDangerous()
	{
		$this->tableCollection->createTable('user')->delete()->sql();
	}

	public function testBasic()
	{
		Assert::same('DELETE FROM user;', $this->tableCollection->createTable('user')->delete()->force()->sql());
	}

	public function testUsing()
	{
		Assert::same('DELETE FROM user USING order, user AS u;', $this->tableCollection->createTable('user')->delete()->force()->using('order')->using('user', 'u')->sql());
	}

}

(new DeleteTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
