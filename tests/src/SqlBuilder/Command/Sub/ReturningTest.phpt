<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../../bootstrap.php';

class ReturningTest extends \Tester\TestCase
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	public function __construct(SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
	}

	public function testInsert()
	{
		Assert::same("INSERT INTO user (name) VALUES ('Milan') RETURNING *;", $this->tableCollection->createTable('user')->insert(['name' => 'Milan'])->returning()->sql());

		Assert::same("INSERT INTO user (name) VALUES ('Milan') RETURNING id, name;", $this->tableCollection->createTable('user')->insert(['name' => 'Milan'])->returning('id, name')->sql());
	}

	public function testInsertAlias()
	{
		Assert::same("INSERT INTO user (name) VALUES ('Doe') RETURNING id AS idecko, name AS main;", $this->tableCollection->createTable('user')->insert(['name' => 'Doe'])->returning('id AS idecko')->returning('name AS main')->sql());
	}

	public function testDelete()
	{
		Assert::same('DELETE FROM user RETURNING *;', $this->tableCollection->createTable('user')->delete()->force()->returning()->sql());

		Assert::same('DELETE FROM user RETURNING id, name;', $this->tableCollection->createTable('user')->delete()->force()->returning('id, name')->sql());
	}

	public function testUpdate()
	{
		Assert::same("UPDATE user SET name='Milan' RETURNING *;", $this->tableCollection->createTable('user')->update(['name' => 'Milan'])->force()->returning()->sql());
		Assert::same("UPDATE user SET name='Milan' RETURNING id, name, 1;", $this->tableCollection->createTable('user')->update(['name' => 'Milan'])->force()->returning('id, name, ?', 1)->sql());
	}

	public function testReset()
	{
		Assert::same("INSERT INTO user (name) VALUES ('Milan');", $this->tableCollection->createTable('user')->insert(['name' => 'Milan'])->returning('id, name')->returning(NULL)->sql());
	}

}

(new ReturningTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
