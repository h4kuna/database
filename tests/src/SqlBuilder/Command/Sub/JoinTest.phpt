<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../../bootstrap.php';

class JoinTest extends \Tester\TestCase
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	/** @var SqlBuilder\Table */
	private $user;

	public function __construct(SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
		$this->user = $this->tableCollection->createTable('user', 'u');
	}

	public function testJoin()
	{
		Assert::equal('SELECT * FROM user AS u JOIN order AS o ON u.id = o.order_id;', $this->user->select()->join('order', 'o')->on('u.id = o.order_id')->sql());
	}

	public function testLeft()
	{
		$this->tableCollection->createTable('info', 'i');
		Assert::equal('SELECT * FROM user AS u JOIN info AS i ON u.id = i.user_id OR id = 1 LEFT JOIN info AS i2 ON i.id = i2.id;', $this->user->select()->join('info', 'i')->on('u.id = i.user_id')->orIf('id', 1)->leftJoin('info', 'i2')->on('i.id = i2.id')->sql());
	}

}

(new JoinTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
