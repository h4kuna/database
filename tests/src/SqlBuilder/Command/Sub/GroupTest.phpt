<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../../bootstrap.php';

class GroupTest extends \Tester\TestCase
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	public function __construct(SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
	}

	public function testBasic()
	{
		Assert::equal('SELECT * FROM user GROUP BY name;', $this->tableCollection->createTable('user')->select()->group('name')->sql());

		Assert::equal('SELECT * FROM user GROUP BY name, 1;', $this->tableCollection->createTable('user')->select()->group('name, ?', 1)->sql());
	}

	public function testResetValue()
	{
		Assert::equal('SELECT * FROM user;', $this->tableCollection->createTable('user')->select()->group('name')->group(NULL)->sql());
	}

}

(new GroupTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
