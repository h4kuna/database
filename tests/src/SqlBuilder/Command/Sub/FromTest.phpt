<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../../bootstrap.php';

class FromTest extends \Tester\TestCase
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	public function __construct(SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
	}

	public function testSelect()
	{
		Assert::same('SELECT name FROM user AS u, order;', $this->tableCollection->createTable('user', 'u')->select('name')->from('order')->sql());
	}

	public function testUpdate()
	{
		// @todo Has UPDATE in FROM alias of tables?
		$this->tableCollection->createTable('orders', 'os');
		Assert::same("UPDATE users AS us SET name='Milan' FROM orders;", $this->tableCollection->createTable('users', 'us')->update(['name' => 'Milan'])->force()->from('orders')->sql());
	}

	public function testDelete()
	{
		$this->tableCollection->createTable('foo', 'f');
		Assert::same('DELETE FROM user AS u, order, foo AS f;', $this->tableCollection->createTable('user', 'u')->delete()->force()->from('order')->from('f')->sql());
	}

}

(new FromTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
