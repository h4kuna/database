<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../../bootstrap.php';

class WhereTest extends \Tester\TestCase
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	/** @var SqlBuilder\Table */
	private $user;

	public function __construct(SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
		$this->user = $this->tableCollection->createTable('user', 'u');
	}

	public function testEqual()
	{
		$user = $this->user;
		Assert::same("SELECT * FROM user AS u WHERE name = 'Milan';", $user->select()->where('name', 'Milan')->sql());
		Assert::same("SELECT * FROM user AS u WHERE name != 'Milan';", $user->select()->where('!name', 'Milan')->sql());
	}

	public function testQuestionMark()
	{
		$user = $this->user;
		Assert::same("SELECT * FROM user AS u WHERE name < 'Milan';", $user->select()->where('name < ?', 'Milan')->sql());
		Assert::same("SELECT * FROM user AS u WHERE name IN ('Milan');", $user->select()->where('name IN ?', ['Milan'])->sql());
	}

	public function testNULL()
	{
		$user = $this->user;
		Assert::same('SELECT * FROM user AS u WHERE name;', $user->select()->where('name')->sql());
		Assert::same('SELECT * FROM user AS u WHERE name IS NULL;', $user->select()->where('name', NULL)->sql());
		Assert::same('SELECT * FROM user AS u WHERE name IS NOT NULL;', $user->select()->where('!name', NULL)->sql());
	}

	public function testMoreParameters()
	{
		$user = $this->user;
		Assert::same('SELECT * FROM user AS u WHERE name IN (3, 5);', $user->select()->where('name IN (?, ?)', 3, 5)->sql());
	}

	public function testSubSelect()
	{
		$order = $this->tableCollection->createTable('order', 'o')->select();

		Assert::same('SELECT * FROM user AS u WHERE (SELECT * FROM order AS o);', $this->user->select()->where($order)->sql());

		Assert::same('SELECT * FROM user AS u WHERE order = (SELECT * FROM order AS o);', $this->user->select()->where('order = ?', $order)->sql());
	}

	public function testMoreCondition()
	{
		Assert::same("SELECT * FROM user AS u WHERE name = 'milan' AND surname = 'matejcek' OR id = 10;", $this->user->select()->where('name', 'milan')->andIf('surname', 'matejcek')->orIf('id', 10)->sql());
	}

	public function testMoreOrCondition()
	{
		Assert::same("SELECT * FROM user AS u WHERE (name = 'milan' OR surname = 'matejcek') AND (id = 10 OR phone = 321654);", $this->user->select()->where('?or', ['name' => 'milan', 'surname' => 'matejcek'])->andIf('?or', ['id' => 10, 'phone' => 321654])->sql());
	}

	/**
	 * @throws \h4kuna\Database\InvalidArgumentException
	 */
	public function testInvalidColumn()
	{
		$this->user->select()->where([])->sql();
	}

	public function testIN()
	{
		$user = $this->user;
		Assert::same("SELECT * FROM user AS u WHERE name IN ('milan', 'joe');", $user->select()->where('name', ['milan', 'joe'])->sql());
		Assert::same("SELECT * FROM user AS u WHERE name NOT IN ('milan', 'joe');", $user->select()->where('!name', ['milan', 'joe'])->sql());
	}

	public function testUpdate()
	{
		Assert::same("UPDATE user AS u SET name='milan' WHERE id = 1;", $this->user->update(['name' => 'milan'])->where('id', 1)->sql());
	}

	public function testDelete()
	{
		Assert::same('DELETE FROM user AS u WHERE id = 1;', $this->user->delete()->where('id', 1)->force()->sql());
	}

	/**
	 * @throws \h4kuna\Database\MethodDoesNotExistsException
	 */
	public function testUnknownMethod()
	{
		$this->user->select()->where('id', 1)->unknownMethod();
	}

	public function testReset()
	{
		Assert::same('DELETE FROM user AS u;', $this->user->delete()->where('id', 1)->where(NULL)->force()->sql());
	}

}

(new WhereTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
