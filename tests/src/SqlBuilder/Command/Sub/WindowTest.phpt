<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../../bootstrap.php';

class WindowTest extends \Tester\TestCase
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	/** @var SqlBuilder\Table */
	private $user;

	public function __construct(SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
		$this->user = $this->tableCollection->createTable('user', 'u');
	}

	public function testBasic()
	{
		Assert::same('SELECT * FROM user AS u WINDOW foo AS bar, bar AS foo;', $this->user->select()->window('foo AS bar')->window('bar AS foo')->sql());

		Assert::same('SELECT * FROM user AS u WINDOW foo AS bar, 1;', $this->user->select()->window('foo AS bar, ?', 1)->sql());
	}

	public function testReset()
	{
		Assert::same('SELECT * FROM user AS u;', $this->user->select()->window('foo AS bar')->window(NULL)->sql());
	}

}

(new WindowTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
