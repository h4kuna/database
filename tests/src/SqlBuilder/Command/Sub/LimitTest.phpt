<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../../bootstrap.php';

class LimitTest extends \Tester\TestCase
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	public function __construct(SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
	}

	public function testSelect()
	{
		Assert::same('SELECT name FROM user LIMIT 10;', $this->tableCollection->createTable('user')->select('name')->limit(10)->sql());

		Assert::same('SELECT name FROM user LIMIT 10 OFFSET 10;', $this->tableCollection->createTable('user')->select('name')->limit(10, 10)->sql());
		Assert::same('SELECT name FROM user;', $this->tableCollection->createTable('user')->select('name')->limit(10, 10)->limit(NULL)->sql());
	}

	public function testSelectOffset()
	{
		Assert::same('SELECT name FROM user OFFSET 10;', $this->tableCollection->createTable('user')->select('name')->offset(10)->sql());
	}

	public function testSelectPage()
	{
		Assert::same('SELECT name FROM user LIMIT 10 OFFSET 10;', $this->tableCollection->createTable('user')->select('name')->limitPage(2, 10)->sql());

		Assert::same('SELECT name FROM user LIMIT 10;', $this->tableCollection->createTable('user')->select('name')->limitPage(-1, 10)->sql());
	}

	public function testResetValue()
	{
		Assert::same('SELECT * FROM user;', $this->tableCollection->createTable('user')->select()->limit(10)->limit(NULL)->sql());
	}

}

(new LimitTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
