<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../../bootstrap.php';

class OrderTest extends \Tester\TestCase
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	public function __construct(SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
	}

	public function testBasic()
	{
		Assert::equal('SELECT * FROM user ORDER BY name DESC;', $this->tableCollection->createTable('user')->select()->order('name DESC')->sql());

		Assert::equal('SELECT * FROM user ORDER BY name DESC, 1;', $this->tableCollection->createTable('user')->select()->order('name DESC, ?', 1)->sql());
	}

	public function testResetValue()
	{
		Assert::equal('SELECT * FROM user;', $this->tableCollection->createTable('user')->select()->order('name DESC')->order(NULL)->sql());
	}

}

(new OrderTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
