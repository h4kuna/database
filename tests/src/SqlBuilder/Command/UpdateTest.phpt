<?php

namespace h4kuna\Database\SqlBuilder\Command;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../bootstrap.php';

class UpdateTest extends \Tester\TestCase
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	public function __construct(SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
	}

	/**
	 * @throws \h4kuna\Database\DangerousQueryException
	 */
	public function testDangerous()
	{
		$this->tableCollection->createTable('user')->update(['name' => 'Milan'])->sql();
	}

	/**
	 * @throws \h4kuna\Database\InvalidArgumentException
	 */
	public function testEmptyData()
	{
		$this->tableCollection->createTable('user')->update([])->force()->sql();
	}

	public function testBasic()
	{
		$table = $this->tableCollection->createTable('user');
		Assert::equal("UPDATE user SET name='Milan' surname='Matějček' settings='{1, 2, 3}';", $table->update(['name' => 'Milan', 'surname' => 'Matějček', 'settings' => [1, 2, 3]])->force()->sql());
	}

}

(new UpdateTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
