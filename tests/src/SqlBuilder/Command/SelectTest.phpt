<?php

namespace h4kuna\Database\SqlBuilder\Command;

use h4kuna\Database\SqlBuilder,
	Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../bootstrap.php';

class SelectTest extends \Tester\TestCase
{

	/** @var SqlBuilder\Table */
	private $table;

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	public function __construct(SqlBuilder\Table $table, SqlBuilder\TableCollection $tableCollection)
	{
		$this->table = $table;
		$this->tableCollection = $tableCollection;
	}

	public function testBasic()
	{
		Assert::equal('SELECT * FROM users;', $this->table->select()->sql());
	}

	public function testColumn()
	{
		Assert::equal('SELECT u.id, u.name FROM users;', $this->table->select('u.id, u.name')->sql());

		Assert::equal('SELECT u.id, u.name, 1 FROM users;', $this->table->select('u.id, u.name, ?', 1)->sql());
	}

	public function testDistinct()
	{
		Assert::equal('SELECT DISTINCT * FROM users;', $this->table->select()->distinct()->sql());

		Assert::equal('SELECT DISTINCT id FROM users;', $this->table->select('id')->distinct()->sql());
	}

	public function testSubSelect()
	{
		$orders = $this->tableCollection->createTable('orders')->select();
		Assert::equal('SELECT * FROM (SELECT * FROM orders) AS s_tmp;', $this->tableCollection->createSubQuery($orders, 's_tmp')->select()->sql());
		Assert::equal('SELECT * FROM (SELECT * FROM orders) AS s_tmp LIMIT 5;', $this->tableCollection->get('s_tmp')->select()->limit(5)->sql());
	}

	public function testUnion()
	{
		$orders = $this->tableCollection->createTable('orders')->select('id, name');
		Assert::equal('SELECT id, name FROM users UNION (SELECT id, name FROM orders);', $this->table->select('id, name')->union($orders)->sql());

		Assert::equal('SELECT id, name FROM users UNION ALL (SELECT id, name FROM orders);', $this->table->select('id, name')->unionAll($orders)->sql());

		Assert::equal('SELECT id, name FROM users INTERSECT (SELECT id, name FROM orders);', $this->table->select('id, name')->intersect($orders)->sql());

		Assert::equal('SELECT id, name FROM users INTERSECT ALL (SELECT id, name FROM orders);', $this->table->select('id, name')->intersectAll($orders)->sql());
	}

	public function testSubSelectFunction()
	{
		$orders = $this->tableCollection->createSubQuery('my_function()', 's_function')->select();
		Assert::equal('SELECT * FROM my_function() AS s_function;', $orders->sql());
		Assert::equal('SELECT * FROM (SELECT * FROM my_function() AS s_function) AS temp;', $this->tableCollection->createSubQuery($orders, 'temp')->select()->sql());
		Assert::equal('SELECT * FROM (SELECT * FROM my_function() AS s_function) AS temp LIMIT 5;', $this->tableCollection->get('temp')->select()->limit(5)->sql());
	}

	public function testAllCommand()
	{
		$sql = $this->table->select('id, DATE_FORMAT(\'%Y-%m-%d\', ?), MAX(visible = ?) AS max', new \DateTime('2016-12-30'), 1)
				->join('user', 'us')->on('us.id = u.id')->andIf('name', 'dou')->orIf('surname', ['foo', 'bar'])
				->leftJoin('group', 'g')->on('g.id < ?', 10)
				->where('g.id')->andIf('?or', ['foo' => 11, 'bar' => 12])
				->window('foo AS bar')
				->group('u.name')
				->having('max > ?', 10)->orIf('max', 5)->orIf('max', [2, 3])
				->order('name ASC NULLS LAST')
				->limit(10, 10)
				->forLock('UPDATE OF user NOWAIT')->sql();
		$expected = "SELECT id, DATE_FORMAT('%Y-%m-%d', '2016-12-30 00:00:00.000000+0100'), MAX(visible = 1) AS max FROM users "
			. "JOIN user AS us ON us.id = u.id AND name = 'dou' OR surname IN ('foo', 'bar') "
			. "LEFT JOIN group AS g ON g.id < 10 "
			. "WHERE g.id AND (foo = 11 OR bar = 12) "
			. "GROUP BY u.name "
			. "HAVING max > 10 OR max = 5 OR max IN (2, 3) "
			. "WINDOW foo AS bar "
			. "ORDER BY name ASC NULLS LAST "
			. "LIMIT 10 OFFSET 10 "
			. "FOR UPDATE OF user NOWAIT;";
		Assert::equal($expected, $sql);
	}

	public function testCount()
	{
		//a..b.(name) AS foo -- .. is join table
		//..b.(name)
		//..b.name
		//author..book.(name)
		$author = $this->tableCollection->createTable('author', 'a');
		$sql = $author->select('id, name, 1 AS one')
			->leftJoin('book', 'b')->on('b.id = a.id')
			->where('!b.name', 'foo')
			->window('foo AS bar')
			->group('b.name')
			->having('one', 1)
			->order('a.name ASC NULLS LAST')
			->limit(10, 10)
			->forLock('UPDATE');
		Assert::equal(0, $sql->count('*'));
		Assert::equal("SELECT COUNT(*) FROM author AS a LEFT JOIN book AS b ON b.id = a.id WHERE b.name != 'foo' GROUP BY b.name;", \h4kuna\Database\Storage\SqlLog::$sql);
	}

	public function testClearLock()
	{
		Assert::same('SELECT * FROM users;', $this->table->select()->forLock('UPDATE')->forLock(NULL)->sql());
	}

}

(new SelectTest($sqlBuilder->getPostgreSql()->getTableCollection()->createTable('users'), $sqlBuilder->getPostgreSql()->getTableCollection()))->run();
