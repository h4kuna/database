<?php

namespace h4kuna\Database\SqlBuilder;

use Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../bootstrap.php';

class TableCollectionTest extends \Tester\TestCase
{

	/** @var TableCollection */
	private $tables;

	public function __construct(TableCollection $tables)
	{
		$this->tables = $tables;
	}

	public function testBasic()
	{
		$table = $this->tables->createTable('user');
		Assert::equal($table, $this->tables->createTable($table->getTable()));
		Assert::equal($table, $this->tables->get('user'));
	}

	public function testTableAlias()
	{
		$table = $this->tables->createTable('users', 'u');
		Assert::equal($table, $this->tables->get($table->getTable()));
		Assert::equal($table, $this->tables->get('u'));
	}

	/**
	 * @throws \h4kuna\Database\DuplicityTableAliasException
	 */
	public function testDuplicity()
	{
		$table1 = $this->tables->createTable('foo', 'f');
		$table1 = $this->tables->createTable('faa', 'f');
	}

	public function testAddTable()
	{
		$table1 = $this->tables->createTable('member', 'm');
		$table2 = $this->tables->createTable('member');
		Assert::equal($table1, $table2);
		Assert::equal($table1, $this->tables->get('m'));
		Assert::equal($table2, $this->tables->get('member'));
	}

	public function testCreateNewTempAlias()
	{
		$table1 = $this->tables->createTable('order', 'o');
		$table2 = $this->tables->createTable('order', 'u');
		Assert::equal('o', $table2->getAlias());
		$table3 = $this->tables->createNewTempAlias('order', 'a');
		Assert::equal('a', $table3->getAlias());
		try {
			$notFoud = FALSE;
			$this->tables->get('a');
		} catch (\h4kuna\Database\TableNotFoundException $e) {
			$notFoud = TRUE;
		}
		Assert::true($notFoud, 'Temporary alias faild.');
	}

	/**
	 * @throws h4kuna\Database\TableNotFoundException
	 */
	public function testAliasNotFound()
	{
		$table1 = $this->tables->createTable('customer');
		Assert::equal($table1, $this->tables->get('c'));
	}

}

(new TableCollectionTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
