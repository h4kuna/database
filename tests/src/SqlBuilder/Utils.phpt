<?php

namespace h4kuna\Database\SqlBuilder;

use Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../bootstrap.php';

class UtilsTest extends \Tester\TestCase
{

	public function testPrepareArguments()
	{
		Assert::equal([], Utils::prepareArguments([]));
		Assert::equal(['*'], Utils::prepareArguments([], ['*']));
		Assert::equal(['foo'], Utils::prepareArguments(['foo']));
		Assert::equal(['foo', ['bar']], Utils::prepareArguments(['foo', 'bar']));
		Assert::equal(['foo', ['bar', ['lala', 'pompo']]], Utils::prepareArguments(['foo', 'bar', ['lala', 'pompo']]));
	}

	public function testIsArray()
	{
		Assert::true(Utils::isArray([]));
		Assert::true(Utils::isArray(new \ArrayIterator()));
		Assert::true(Utils::isArray(new \ArrayObject()));

		Assert::false(Utils::isArray(new \stdClass()));
	}

}

(new UtilsTest)->run();

