<?php

namespace h4kuna\Database\SqlBuilder;

use Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../bootstrap.php';

class TableTest extends \Tester\TestCase
{

	/** @var TableCollection */
	private $tableCollection;

	public function __construct(TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
	}

	public function testBasic()
	{
		$user = $this->tableCollection->createTable('user');
		Assert::equal('user', $user->getTable());
		Assert::equal(NULL, $user->getAlias());
		Assert::equal('user', $user->sql());
		Assert::equal('user', (string) $user);
	}

	public function testAlias()
	{
		$users = $this->tableCollection->createTable('users', 'u');
		Assert::equal('users AS u', $users->sql());
		Assert::equal('users AS u', (string) $users);
		Assert::equal('users', $users->getTable());
		Assert::equal('u', $users->getAlias());
	}

	public function testField()
	{
		$user = $this->tableCollection->createTable('user');
		Assert::equal('user.name', $user->createField('name'));
		Assert::equal('user.name user.id', $user->createField('[name] [id]'));

		$users = $this->tableCollection->createTable('users', 'u');
		Assert::equal('u.name', $users->createField('name'));
		Assert::equal('u.name u.id', $users->createField('[name] [id]'));
	}

	public function testFields()
	{
		$users = $this->tableCollection->createTable('users', 'u');
		Assert::equal(['u.name', 'u.id'], $users->createFields(['name', 'id']));
		Assert::equal(['u.name', 'u.id'], $users->createFields('name', 'id'));
	}

}

(new TableTest($sqlBuilder->getPostgreSql()->getTableCollection()))->run();
