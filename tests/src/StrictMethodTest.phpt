<?php

namespace h4kuna\Database;

use Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
require __DIR__ . '/../bootstrap.php';

class StrictMethodTest extends \Tester\TestCase
{

	/** @var string[] */
	private $filter;

	/** @var string[] */
	private $condition;

	/**
	 * @dataProvider StrictMethods-checkCondition.ini
	 */
	public function testCheckCondition($name)
	{
		$conditionMethods = $this->getConditionMethods();
		$class = new \ReflectionClass(__NAMESPACE__ . '\SqlBuilder\Command\\' . $name);
		$methods = $this->filterMethods($class->getMethods());

		Assert::same([], array_intersect($conditionMethods, $methods));
	}

	/**
	 * @return string[]
	 */
	private function getConditionMethods()
	{
		if ($this->condition) {
			return $this->condition;
		}
		$class = new \ReflectionClass(__NAMESPACE__ . '\SqlBuilder\Statement');
		return $this->condition = $this->filterMethods($class->getMethods());
	}

	private function filterMethods(array $methods)
	{
		$out = [];
		foreach ($methods as $method) {
			if (in_array($method->name, $this->getFilter())) {
				continue;
			}
			$out[] = $method->name;
		}
		return $out;
	}

	private function getFilter()
	{
		if ($this->filter) {
			return $this->filter;
		}
		$interface = new \ReflectionClass(__NAMESPACE__ . '\SqlBuilder\CommandInterface');
		$filter = ['__call', '__construct', '__toString', '__get', '__set'];
		foreach ($interface->getMethods() as $method) {
			$filter[] = $method->name;
		}
		return $this->filter = $filter;
	}

}

(new StrictMethodTest())->run();
