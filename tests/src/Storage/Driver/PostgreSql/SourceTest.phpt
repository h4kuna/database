<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use Tester\Assert;

/* @var $builder \h4kuna\Database\Test\SqlBuilder */
$builder = require __DIR__ . '/../../../../bootstrap.php';

class SourceTest extends \Tester\TestCase
{

	/** @var Query */
	private $query;

	public function __construct(Query $query)
	{
		$this->query = $query;
	}

	public function testCount()
	{
		$data = $this->query->execute("INSERT INTO author (id, name) VALUES (1, ?), (2, 'joe'), (3, 'ali') RETURNING *", 'doe');

		Assert::equal(3, $data->count());
		Assert::equal(3, count($data));

		// zero
		$data = $this->query->execute("INSERT INTO author (id, name) VALUES (4, ?)", 'doe');

		Assert::equal([], $data->fetchAll());
		Assert::equal(0, count($data));
	}

	/**
	 * @throws \h4kuna\Database\DumplicityColumnNameException
	 */
	public function testBasic()
	{
		$data = $this->query->execute("INSERT INTO author (id, name) VALUES (1, ?)", 'doe');

		$sql = $this->query->execute('SELECT id, name, id FROM author WHERE id = 1');
		foreach ($sql as $row) {

		}
	}

	protected function tearDown()
	{
		$this->query->execute('DELETE FROM author WHERE id IN (1, 2, 3, 4)');
	}

}

(new SourceTest($builder->getPostgreSql()->getQuery()))->run();
