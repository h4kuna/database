<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use Tester\Assert;

/* @var $builder \h4kuna\Database\Test\SqlBuilder */
$builder = require __DIR__ . '/../../../../bootstrap.php';

class TransactionTest extends \Tester\TestCase
{

	/** @var Transaction */
	private $transaction;

	public function __construct(Transaction $transaction)
	{
		$this->transaction = $transaction;
	}

	function testBasic()
	{
		Assert::equal(1, $this->transaction->begin());
		Assert::true($this->transaction->inTransaction());
		Assert::equal(0, $this->transaction->commit());
		$this->checkNotInTransaction();
		Assert::equal(1, $this->transaction->begin());
		// fail
		Assert::equal(0, $this->transaction->rollback());
		$this->checkNotInTransaction();
	}

	function testSavepoint()
	{
		Assert::equal(1, $this->transaction->begin());
		Assert::equal(2, $this->transaction->begin());
		Assert::equal(1, $this->transaction->commit());
		Assert::equal(0, $this->transaction->commit());
		$this->checkNotInTransaction();
		Assert::equal(1, $this->transaction->begin());
		Assert::equal(2, $this->transaction->begin());
		Assert::equal(1, $this->transaction->rollback());
		Assert::equal(0, $this->transaction->commit());
		$this->checkNotInTransaction();
	}

	function testFail()
	{
		$transaction = $this->transaction;
		Assert::exception(function() use ($transaction) {
			$transaction->commit();
		}, 'h4kuna\Database\InvalidStateException');
		Assert::exception(function() use ($transaction) {
			$transaction->rollback();
		}, 'h4kuna\Database\InvalidStateException');
	}

	private function checkNotInTransaction()
	{
		Assert::false($this->transaction->inTransaction());
	}

}

$transaction = $builder->getPostgreSql()->getTransaction();

(new TransactionTest($transaction))->run();
