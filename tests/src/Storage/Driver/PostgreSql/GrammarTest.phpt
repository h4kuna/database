<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use Tester\Assert,
	h4kuna\Database\Storage\Driver,
	h4kuna\Database\SqlBuilder;

/* @var $builder \h4kuna\Database\Test\SqlBuilder */
$builder = require __DIR__ . '/../../../../bootstrap.php';

class GrammarTest extends \Tester\TestCase
{

	/** @var Grammar */
	private $grammar;

	public function __construct()
	{
		$this->grammar = new Grammar;
	}

	public function testString()
	{
		Assert::equal("'Joe''s Widgets'", $this->grammar->queryValue('Joe\'s Widgets'));
	}

	public function testNumeric()
	{
		Assert::equal(1, $this->grammar->queryValue(1));
		Assert::equal(1, $this->grammar->queryValue('1'));
		Assert::equal(1.1, $this->grammar->queryValue(1.1));
		Assert::equal(1.1, $this->grammar->queryValue('1.1'));
	}

	public function testDatetime()
	{
		Assert::equal("'1986-12-30 06:30:58.789000+0200'", $this->grammar->queryValue(new \DateTime('1986-12-30 06:30:58.789+02')));
		Assert::equal("'1986-12-30 00:00:00.000000+0100'", $this->grammar->queryValue(new \DateTime('1986-12-30')));
	}

	public function testNULL()
	{
		Assert::equal('NULL', $this->grammar->queryValue(NULL));
	}

	public function testLiteral()
	{
		Assert::equal('NOW()', $this->grammar->queryValue(new SqlBuilder\Literal('NOW()')));
	}

	public function testJson()
	{
		Assert::equal('\'{"name":"Doe","surname":"Joe"}\'', $this->grammar->queryValue(new Json()));
	}

	public function testArray()
	{
		Assert::equal("('Joe''s Widgets', 1, 2, 3.3, 4.4, 'bar', '1986-12-30 00:00:00.000000+0100')", $this->grammar->queryValue(['Joe\'s Widgets', '1', 2, 3.3, '4.4', 'bar', new \DateTime('1986-12-30')]));

		Assert::equal('\'{"Joe\'\'s \"\\\\Widgets", 1, 2, 3.3, 4.4, "bar", "1986-12-30 00:00:00.000000+0100", {"Joe\'\'s \"\\\\Widgets2", 1}}\'', $this->grammar->queryValue(['Joe\'s "\Widgets', '1', 2, 3.3, '4.4', 'bar', new \DateTime('1986-12-30'), ['Joe\'s "\Widgets2', 1]], TRUE));
	}

	public function testMarkParameter()
	{
		Assert::same('$foo', $this->grammar->markParameter('foo'));
	}

	public function testDatetimeFromDb()
	{
		$actual = $this->grammar->normalizeValue('1986-12-30 05:06:07.08000', Driver\GrammarInterface::TYPE_DATETIME);
		Assert::equal(self::format('1986-12-30 05:06:07.080000'), self::format($actual));
		$actual = $this->grammar->normalizeValue('1986-12-30', Driver\GrammarInterface::TYPE_DATE);
		Assert::equal(self::format('1986-12-30'), self::format($actual));
		$actual = $this->grammar->normalizeValue('12:30:00.00', Driver\GrammarInterface::TYPE_TIME);
		Assert::equal(self::format('1970-01-01 12:30:00'), self::format($actual));

		$actual = $this->grammar->normalizeValue('1986-12-30 05:06:07.08000+05', Driver\GrammarInterface::TYPE_DATETIME_TZ);
		Assert::equal(self::format('1986-12-30 05:06:07.08000+05', TRUE), self::format($actual));
	}

	public function testBool()
	{
		Assert::equal($this->grammar->queryValue(TRUE), "'t'");
		Assert::equal($this->grammar->queryValue(FALSE), "'f'");
		Assert::true($this->grammar->normalizeValue('t', Grammar::TYPE_BOOL));
		Assert::false($this->grammar->normalizeValue('f', Grammar::TYPE_BOOL));
	}

	public function testNormalizeArray()
	{
		Assert::equal([1, 2, 3], $this->grammar->normalizeValue('{1,2,3}', Driver\GrammarInterface::TYPE_ARRAY | Driver\GrammarInterface::TYPE_INT));
		Assert::equal(['Joe', 'Hello world'], $this->grammar->normalizeValue('{Joe,"Hello world"}', Driver\GrammarInterface::TYPE_ARRAY | Driver\GrammarInterface::TYPE_STRING));

		// unsupported multidimensional array
		Assert::equal([[4, 5], [6, 7]], $this->grammar->normalizeValue('{{4,5},{6,7}}', Driver\GrammarInterface::TYPE_ARRAY | Driver\GrammarInterface::TYPE_INT));
		// Assert::equal([['Joe', 'Doe'], ['Hello world', 'Good']], $this->grammar->normalizeValue('{{Joe,Doe},{"Hello world",Good}}', Driver\GrammarInterface::TYPE_ARRAY | Driver\GrammarInterface::TYPE_STRING));
	}

	private static function format($value, $tz = FALSE)
	{
		if (is_string($value)) {
			if ($tz === TRUE) {
				$value = date_create_from_format(Grammar::DATE_FORMAT, $value);
			} else {
				$value = new \DateTime($value);
			}
		}
		return $value->format(Grammar::DATE_FORMAT);
	}

}

class Json implements \JsonSerializable
{

	public $name = 'Doe';
	public $surname = 'Joe';

	public function jsonSerialize()
	{
		return \Nette\Utils\Json::encode(get_object_vars($this));
	}

}

(new GrammarTest)->run();
