<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use Tester\Assert,
	h4kuna\Database,
	h4kuna\Database\SqlBuilder;

/* @var $builder \h4kuna\Database\Test\SqlBuilder */
$builder = require __DIR__ . '/../../../../bootstrap.php';

class QueryTest extends \Tester\TestCase
{

	/** @var Query */
	private $query;

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	public function __construct(Query $query, SqlBuilder\TableCollection $tableCollection)
	{
		$this->query = $query;
		$this->tableCollection = $tableCollection;
	}

	public function testSql()
	{
		$insert = $this->query->execute('INSERT INTO author (id, name) VALUES (5, ?), (6, ?) RETURNING *;', 'doe', 'joe');

		$count = 0;
		foreach ($insert as $row) {
			++$count;
			Assert::true($row instanceof \h4kuna\Database\Result\RowInterface);
		}
	}

	public function testCommand()
	{
		$author = $this->tableCollection->createTable('author');
		$insert = $author->insert(['id' => 5, 'name' => 'doe'])->addRow(['id' => 6, 'name' => 'joe'])->returning();
		/* @var $result \h4kuna\Database\Result\RowCollection */
		$result = $this->query->execute($insert);
		Assert::same(2, $result->count());
	}

	/**
	 * @throws \h4kuna\Database\TableAlreadyExistsException
	 */
	public function testDuplicityTable()
	{
		$this->query->execute('CREATE TABLE author ( id serial NOT NULL )');
	}

	/**
	 * @throws \h4kuna\Database\NotNullConstraintViolationException
	 */
	public function testNullNotAllowed()
	{
		$this->query->execute('INSERT INTO author (id) VALUES (5)');
	}

	/**
	 * @throws \h4kuna\Database\UniqueConstraintViolationException
	 */
	public function testInsertDuplicity()
	{
		$this->query->execute('INSERT INTO author (id, name, "unique") VALUES (5, ?, 1)', 'doe');
		try {
			$this->query->execute('INSERT INTO author (id, name, "unique") VALUES (6, ?, 1)', 'joe');
		} catch (Database\UniqueConstraintViolationException $e) {
			Assert::same(['unique'], $e->getColumns());
			Assert::same(['1'], $e->getValues());
			Assert::same(['unique' => '1'], $e->getUnique());
			throw $e;
		}
	}

	/**
	 * @throws \h4kuna\Database\UniqueConstraintViolationException
	 */
	public function testInsertDuplicityPrimary()
	{
		$this->query->execute('INSERT INTO author (id, name) VALUES (5, ?)', 'doe');
		try {
			$this->query->execute('INSERT INTO author (id, name) VALUES (5, ?)', 'joe');
		} catch (Database\UniqueConstraintViolationException $e) {
			Assert::same(['id'], $e->getColumns());
			Assert::same(['5'], $e->getValues());
			Assert::same(['id' => '5'], $e->getUnique());
			throw $e;
		}
	}

	/**
	 * @throws \h4kuna\Database\UniqueConstraintViolationException
	 */
	public function testInsertDuplicityMultiColumns()
	{
		$this->query->execute('INSERT INTO author (id, name) VALUES (6, ?)', 'doe');
		$this->query->execute('INSERT INTO book (id, name) VALUES (5, ?)', 'doe\'s biography');

		$this->query->execute('INSERT INTO author_x_book (author_id, book_id) VALUES (6, 5)');
		try {
			$this->query->execute('INSERT INTO author_x_book (author_id, book_id) VALUES (6, 5)');
		} catch (Database\UniqueConstraintViolationException $e) {
			Assert::same(['author_id', 'book_id'], $e->getColumns());
			Assert::same(['6', '5'], $e->getValues());
			Assert::same(['author_id' => '6', 'book_id' => '5'], $e->getUnique());
			throw $e;
		}
	}

	/**
	 * @throws \h4kuna\Database\ForeingKeyContraintViolationException
	 */
	public function testDeleteContraintForeingKey()
	{
		$this->query->execute('INSERT INTO author (id, name) VALUES (6, ?)', 'doe');
		$this->query->execute('INSERT INTO book (id, name) VALUES (5, ?)', 'doe\'s biography');

		$this->query->execute('INSERT INTO author_x_book (author_id, book_id) VALUES (6, 5)');
		try {
			$this->query->execute('DELETE FROM author WHERE id IN (6)');
		} catch (Database\ForeingKeyContraintViolationException $e) {
			Assert::same(['id'], $e->getColumns());
			Assert::same(['6'], $e->getValues());
			Assert::same(['id' => '6'], $e->getUnique());
			throw $e;
		}
	}

	/**
	 * @throws \h4kuna\Database\ForeingKeyContraintViolationException
	 */
	public function testInsertForeingKey()
	{
		try {
			$this->query->execute('INSERT INTO author_x_book (author_id, book_id) VALUES (6, 5)');
		} catch (Database\ForeingKeyContraintViolationException $e) {
			Assert::same(['author_id'], $e->getColumns());
			Assert::same(['6'], $e->getValues());
			Assert::same(['author_id' => '6'], $e->getUnique());
			throw $e;
		}
	}

	/**
	 * @throws \h4kuna\Database\InvalidArgumentException
	 */
	public function testInvalidArgument()
	{
		$this->query->execute([]);
	}

	/**
	 * @throws \h4kuna\Database\QueryException
	 */
	public function testContraint()
	{
		$this->query->execute('SELECT unknown FROM author');
	}

	protected function tearDown()
	{
		$this->query->execute('DELETE FROM author_x_book WHERE author_id = 6 AND book_id = 5');
		$this->query->execute('DELETE FROM author WHERE id IN (5, 6)');
		$this->query->execute('DELETE FROM book WHERE id IN (5, 6)');
	}

}

(new QueryTest($builder->getPostgreSql()->getQuery(), $builder->getPostgreSql()->getTableCollection()))->run();
