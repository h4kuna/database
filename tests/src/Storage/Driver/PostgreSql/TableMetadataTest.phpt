<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use Tester\Assert;

/* @var $builder \h4kuna\Database\Test\SqlBuilder */
$builder = require __DIR__ . '/../../../../bootstrap.php';

class TableMetadataTest extends \Tester\TestCase
{

	/** @var \h4kuna\Database\SqlBuilder\TableCollection */
	private $tableCollection;

	public function __construct(\h4kuna\Database\SqlBuilder\TableCollection $tableCollection)
	{
		$this->tableCollection = $tableCollection;
	}

	/**
	 * @dataProvider TableMetadata-table.ini
	 */
	public function testTable($table, $serialized, $versionPHP)
	{
		if (substr(PHP_VERSION_ID, 0, -2) != $versionPHP) {
			return;
		}

		$author = $this->tableCollection->createTable($table);
		Assert::equal($author->getMetadata(), $this->tableCollection->get($table)->getMetadata());
		Assert::equal($serialized, serialize($author->getMetadata()));
	}

}

(new TableMetadataTest($builder->getPostgreSql()->getTableCollection()))->run();
