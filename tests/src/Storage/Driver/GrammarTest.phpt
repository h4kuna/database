<?php

namespace h4kuna\Database\Storage\Driver;

use Tester\Assert;

/* @var $sqlBuilder \h4kuna\Database\Test\SqlBuilder */
$sqlBuilder = require __DIR__ . '/../../../bootstrap.php';

class GrammarTest extends \Tester\TestCase
{

	public function testColumns()
	{
		$grammar = new \h4kuna\Database\Test\GrammerNoSql();
		$columns = range(0, 5);
		Assert::same('[0], [1], [2], [3], [4], [5]', $grammar->prepareColumns($columns));
	}

	public function testValues()
	{
		$grammar = new \h4kuna\Database\Test\GrammerNoSql();
		$values = range(0, 5);
		Assert::same('("0", "1", "2", "3", "4", "5")', $grammar->prepareValues($values));
	}

}

(new GrammarTest)->run();
