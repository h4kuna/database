<?php

namespace h4kuna\Database\Result;

use Tester\Assert,
	h4kuna\Database\Storage\Driver\PostgreSql;

/* @var $builder \h4kuna\Database\Test\SqlBuilder */
$builder = require __DIR__ . '/../../bootstrap.php';

class RowCollectionTest extends \Tester\TestCase
{

	/** @var PostgreSql\Query */
	private $query;

	public function __construct(PostgreSql\Query $query)
	{
		$this->query = $query;
	}

	/**
	 * @dataProvider RowCollection-fetchAll.ini
	 */
	public function testFetch($method, $value, $key, $result)
	{
		if ($value === '') {
			$value = NULL;
		}
		if ($key === '') {
			$key = NULL;
		}

		$rows = $this->query->execute('INSERT INTO author (id, name) VALUES (10, ?), (11, ?), (12, ?) RETURNING id, name', 'doe', 'joe', 'foo');

		Assert::same($result, serialize($rows->{$method}($key, $value)));
	}

	public function testFetchSingle()
	{
		$this->query->execute('INSERT INTO author (id, name) VALUES (10, ?), (11, ?), (12, ?)', 'doe', 'joe', 'foo');

		$data = $this->query->execute('SELECT COUNT(*) FROM author WHERE id IN (10, 11, 12)');
		Assert::same(3, $data->fetchSingle());
	}

	protected function tearDown()
	{
		$this->query->execute('DELETE FROM author WHERE id IN ?', [10, 11, 12]);
	}

}

(new RowCollectionTest($builder->getPostgreSql()->getQuery()))->run();
