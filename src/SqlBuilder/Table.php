<?php

namespace h4kuna\Database\SqlBuilder;

use h4kuna\Database,
	h4kuna\Database\Storage\Driver,
	h4kuna\Database\SqlBuilder\Command;

class Table
{

	/** @var Driver\GrammarInterface */
	private $grammar;

	/** @var TableCollection */
	private $tableCollection;

	/** @var Driver\QueryInterface */
	private $query;

	/** @var string|Command\Select */
	private $table;

	/** @var StatementFactory */
	private $statementFactory;

	/** @var Driver\TableMetadataInterface */
	private $tableMetadata;

	/** @var string */
	private $alias;

	public function __construct(Driver\GrammarInterface $grammar, TableCollection $tableCollection, Driver\QueryInterface $query, StatementFactory $statementFactory, Driver\TableMetadataInterface $tableMetadata, $name, $alias = NULL)
	{
		if (!is_string($name) && !($name instanceof Command\Select)) {
			throw new Database\InvalidArgumentException('Must be string or Select object.');
		}
		$this->query = $query;
		$this->statementFactory = $statementFactory;
		$this->grammar = $grammar;
		$this->tableCollection = $tableCollection;
		$this->tableMetadata = $tableMetadata;
		$this->table = $name;
		$this->alias = $alias;
	}

	public function getAlias()
	{
		return $this->alias;
	}

	public function setAlias($alias)
	{
		$table = clone $this;
		$table->alias = $alias;
		return $table;
	}

	/**
	 * @param array|string $fields
	 * @param string optional
	 * @return array
	 */
	public function createFields($fields /* , ... */)
	{
		if (!is_array($fields)) {
			$fields = func_get_args();
		}
		$out = [];
		foreach ($fields as $field) {
			$out[] = $this->createField($field);
		}
		return $out;
	}

	public function createField($field)
	{
		$count = 0;
		$replace = preg_replace_callback('~(\[(.*)\])~U', [$this, 'fieldReplace'], $field, -1, $count);
		if ($count) {
			return $replace;
		}
		return $this->prependAlias($field);
	}

	public function getTable()
	{
		return $this->table;
	}

	public function sql($showAlias = TRUE)
	{
		if ($this->table instanceof Command\Select) {
			return '(' . $this->table->sql() . ') AS ' . $this->table->getAlias();
		}
		return $this->grammar->queryTable($this, $showAlias);
	}

	/**
	 * @param array $data
	 * @return Command\Insert
	 */
	public function insert($data)
	{
		return (new Command\Insert($this, $this->grammar, $this->tableCollection, $this->query, $this->statementFactory))->addRow($data);
	}

	public function select($column = NULL, $values = NULL /* ... */)
	{
		$select = new Command\Select($this, $this->grammar, $this->tableCollection, $this->query, $this->statementFactory);
		if ($column !== NULL) {

			call_user_func_array([$select, 'column'], func_get_args());
		}
		return $select;
	}

	public function delete()
	{
		return new Command\Delete($this, $this->grammar, $this->tableCollection, $this->query, $this->statementFactory);
	}

	public function update($data)
	{
		return (new Command\Update($this, $this->grammar, $this->tableCollection, $this->query, $this->statementFactory))->setData($data);
	}

	public function getMetadata()
	{
		return $this->tableMetadata->getMetadata($this->getTable());
	}

	private function prependAlias($field)
	{
		if ($this->alias) {
			return $this->alias . '.' . $field;
		}
		return $this->table . '.' . $field;
	}

	private function fieldReplace($found)
	{
		return $this->prependAlias($found[2]);
	}

	public function __toString()
	{
		return $this->sql();
	}

}
