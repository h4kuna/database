<?php

namespace h4kuna\Database\SqlBuilder;

class Literal
{

	/** @var string */
	private $command;

	public function __construct($command)
	{
		$this->command = $command;
	}

	public function getCommand()
	{
		return $this->command;
	}

}
