<?php

namespace h4kuna\Database\SqlBuilder;

use h4kuna\Database,
	h4kuna\Database\SqlBuilder,
	h4kuna\Database\Storage\Driver;

class TableCollection
{

	/** @var Driver\GrammarInterface */
	private $grammar;

	/** @var Driver\QueryInterface */
	private $query;

	/** @var StatementFactory */
	private $statementFactory;

	/** @var Driver\TableMetadataInterface */
	private $tableMetadata;

	/** @var SqlBuilder\Table[] */
	private $aliases = [];

	public function __construct(Driver\GrammarInterface $grammar, Driver\QueryInterface $query, StatementFactory $statementFactory, Driver\TableMetadataInterface $tableMetadata)
	{
		$this->grammar = $grammar;
		$this->query = $query;
		$this->statementFactory = $statementFactory;
		$this->tableMetadata = $tableMetadata;
	}

	/**
	 * @param SqlBuilder\Table $table
	 * @return Table
	 * @throws Database\DuplicityTableAliasException
	 */
	public function addTable(Table $table)
	{
		$tableExists = isset($this->aliases[$table->getTable()]);
		$aliasExists = isset($this->aliases[$table->getAlias()]);
		if ($aliasExists && !$tableExists) {
			throw new Database\DuplicityTableAliasException($table->getAlias());
		} elseif (!$tableExists) {
			$originTable = $this->aliases[$table->getTable()] = $table;
		} else {
			$originTable = $this->aliases[$table->getTable()];
		}
		if (!$aliasExists && $table->getAlias()) {
			$this->aliases[$table->getAlias()] = $originTable;
		}
		return $originTable;
	}

	/**
	 * @param string|Table $alias
	 * @return SqlBuilder\Table
	 * @throws Database\TableNotFoundException
	 */
	public function get($alias)
	{
		if ($alias instanceof Table) {
			return $alias;
		} elseif (!isset($this->aliases[$alias])) {
			throw new Database\TableNotFoundException($alias . ' Let\' register table via createTable().');
		}

		return $this->aliases[$alias];
	}

	/**
	 * @param string $table
	 * @param string|NULL $alias
	 * @return Table
	 */
	public function createTable($table, $alias = NULL)
	{
		return $this->addTable($this->prepareTable($table, $alias));
	}

	/**
	 * @param string $table
	 * @param string $alias
	 * @return Table
	 */
	public function createSubQuery($table, $alias)
	{
		$tableObject = $this->prepareTable($table, $alias);
		if (isset($this->aliases[$alias])) {
			throw new Database\DuplicityTableAliasException($alias);
		}
		return $this->aliases[$alias] = $tableObject;
	}

	/**
	 * @param string $name
	 * @param string|NULL $newAlias
	 * @return Table
	 */
	public function createNewTempAlias($name, $newAlias)
	{
		try {
			$newAliasTable = $table = $this->get($name);
			if ($newAlias) {
				$newAliasTable = $table->setAlias($newAlias);
			}
		} catch (Database\TableNotFoundException $e) {
			$newAliasTable = $this->createTable($name, $newAlias);
		}
		return $newAliasTable;
	}

	private function prepareTable($table, $alias)
	{
		return new Table($this->grammar, $this, $this->query, $this->statementFactory, $this->tableMetadata, $table, $alias);
	}

}
