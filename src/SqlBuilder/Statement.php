<?php

namespace h4kuna\Database\SqlBuilder;

use h4kuna\Database,
	h4kuna\Database\SqlBuilder,
	h4kuna\Database\Storage\Driver;

/**
 * @todo test
 * AND ...
 * OR ...
 */
class Statement implements SqlBuilder\CommandInterface
{

	const
		COND_AND = 1,
		COND_OR = 2;

	/** @var Driver\GrammarInterface */
	private $grammar;

	/** @var string */
	private $column;

	/** @var string|int|array|NULL|\Datatime|\stdClass */
	private $values = [];

	/** @var int */
	private $prev = 0;

	/** @var bool */
	private $conditionArray = FALSE;

	/** @var int */
	private $count = 0;

	public function __construct(Driver\GrammarInterface $grammar)
	{
		$this->grammar = $grammar;
	}

	/**
	 * @param string $column
	 * @return self
	 * @throws Database\InvalidArgumentException
	 */
	public function setColumn($column)
	{
		$this->column = $column;
		return $this;
	}

	/**
	 * @param mixed $values
	 * @return self
	 */
	public function setValues(array $values)
	{
		$this->values = $values;
		$this->conditionArray = current($values) instanceof self;
		return $this;
	}

	public function isEmpty()
	{
		if ($this->conditionArray === TRUE && $this->values !== []) {
			return FALSE;
		}
		return $this->column === NULL;
	}

	public function orIf()
	{
		$this->prev = self::COND_OR;
		return $this;
	}

	public function andIf()
	{
		$this->prev = self::COND_AND;
		return $this;
	}

	public function getQuery()
	{

	}

	public function sql()
	{
		if (!$this->column === NULL) {
			return '';
		} elseif ($this->column instanceof SqlBuilder\Command\Select) {
			return $this->padSql('(' . $this->column->setSubSelect()->sql() . ')');
		} elseif ($this->values === []) {
			return $this->padSql($this->column);
		} elseif ($this->conditionArray) {
			return $this->padSql('(' . substr(implode($this->values), 1) . ')');
		} else {
			foreach ($this->values as $k => $value) {
				if ($value instanceof SqlBuilder\Command\Select) {
					$this->values[$k] = '(' . $value->setSubSelect()->sql() . ')';
				} else {
					$this->values[$k] = $this->grammar->queryValue($value);
				}
			}
		}

		$this->count = 0;
		return $this->padSql(preg_replace_callback('~\?~', [$this, 'stackReplace'], $this->grammar->queryColumn($this->column)));
	}

	private function stackReplace()
	{
		if (isset($this->values[$this->count])) {
			return $this->values[$this->count++];
		}
		throw new Database\InvalidArgumentException('Too few argument for sql.');
	}

	private function padSql($sql)
	{
		return $this->getPrev() . ' ' . $sql;
	}

	private function getPrev()
	{
		if ($this->prev === self::COND_AND) {
			return ' AND';
		} elseif ($this->prev === self::COND_OR) {
			return ' OR';
		}
		return '';
	}

	public function __toString()
	{
		return $this->sql();
	}

}
