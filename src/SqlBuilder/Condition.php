<?php

namespace h4kuna\Database\SqlBuilder;

use h4kuna\Database,
	h4kuna\Database\SqlBuilder;

/**
 * FROM table WHERE {condition}
 * JOIN table ON {condition}
 */
class Condition implements SqlBuilder\CommandInterface
{

	/** @var SqlBuilder\SUIDAbstract */
	private $command;

	/** @var StatementCollection */
	private $statementCollesction;

	public function __construct(SqlBuilder\SUIDAbstract $command, StatementCollection $statementCollesction)
	{
		$this->command = $command;
		$this->statementCollesction = $statementCollesction;
	}

	public function __call($name, $arguments)
	{
		if (method_exists($this->command, $name)) {
			return call_user_func_array([$this->command, $name], $arguments);
		}

		throw new Database\MethodDoesNotExistsException(get_class($this->command) . '::' . $name);
	}

	public function andIf($column, $values = [] /* ... */)
	{
		$condition = $this->statementCollesction->createStatement();
		$this->statementCollesction->isEmpty() || $condition->andIf();
		if (!$this->isMultiCondition($column)) {
			$values = func_get_args();
			array_shift($values);
		}

		return $this->prepareValues($condition, $column, $values);
	}

	public function orIf($column, $values = [] /* ... */)
	{
		$condition = $this->statementCollesction->createStatement();
		$this->statementCollesction->isEmpty() || $condition->orIf();
		if (!$this->isMultiCondition($column)) {
			$values = func_get_args();
			array_shift($values);
		}

		return $this->prepareValues($condition, $column, $values);
	}

	public function end()
	{
		return $this->command;
	}

	public function isEmpty()
	{
		return $this->statementCollesction->isEmpty();
	}

	private function prepareValues(Statement $condition, $column, array $args)
	{
		$this->prepareCondition($condition, $column, $args);
		$this->statementCollesction->append($condition);
		return $this;
	}

	public function getCommand()
	{
		return $this->command;
	}

	public function getQuery()
	{

	}

	/**
	 * @param bool $parent Hack for fluent :) do not use.
	 * @return string
	 */
	public function sql($parent = TRUE)
	{
		if ($parent) {
			return $this->command->sql();
		}

		return $this->statementCollesction->sql('');
	}

	private function prepareCondition(Statement $condition, $column, $values)
	{
		if ($column === NULL) {
			$this->statementCollesction->add(NULL);
			return $this;
		}
		if ($this->isMultiCondition($column)) {
			if (!SqlBuilder\Utils::isArray($values)) {
				throw new Database\InvalidArgumentException('Values must be added like array. [\'foo > ?\' => 1, \'!bar\' => 2]');
			}

			$conditions = $this->recursiveCondition($column === '?or' ? $condition::COND_OR : $condition::COND_AND, $values);
			return $condition->setValues($conditions);
		} elseif ($column instanceof SqlBuilder\Command\Select) {
			return $condition->setColumn($column);
		} elseif (!is_string($column)) {
			throw new Database\InvalidArgumentException('Column is only string, without variables. Add variables by "?" a question mark.');
		} elseif (!$values) {
			return $condition->setColumn($column);
		}

		if (strrpos($column, '?') === FALSE) {
			$negation = FALSE;
			if (substr($column, 0, 1) === '!') {
				$column = substr($column, 1);
				$negation = TRUE;
			}
			$column .= $this->autoFillQuestion($values, $negation);
		}

		return $condition->setColumn($column)->setValues((array) $values);
	}

	private function isMultiCondition($column)
	{
		return $column === '?or' || $column === '?and';
	}

	private function recursiveCondition($join, $values)
	{
		$conditions = [];
		$i = 0;
		foreach ($values as $column => $value) {
			$condition = $this->statementCollesction->createStatement();
			if ($i) {
				if ($join === $condition::COND_AND) {
					$condition->andIf();
				} else {
					$condition->orIf();
				}
			}
			$conditions[] = $this->prepareCondition($condition, $column, $value);
			++$i;
		}
		return $conditions;
	}

	private function autoFillQuestion($values, $negation)
	{
		$column = '';
		if ($values === [NULL]) {
			$column .= ' IS';
			if ($negation) {
				$column .= ' NOT';
			}
			$column .= ' ?';
		} elseif (SqlBuilder\Utils::isArray($values) && SqlBuilder\Utils::isArray(current($values))) {
			if ($negation) {
				$column .= ' NOT';
			}
			$column .= ' IN ?';
		} elseif ($negation) {
			$column .=' != ?';
		} else {
			$column .= ' = ?';
		}
		return $column;
	}

}
