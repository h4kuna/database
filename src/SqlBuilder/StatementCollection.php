<?php

namespace h4kuna\Database\SqlBuilder;

class StatementCollection implements CommandInterface
{

	/** @var StatementFactory */
	private $statementFactory;

	/** @var Statement[] */
	private $collection = [];

	public function __construct(StatementFactory $statementFactory)
	{
		$this->statementFactory = $statementFactory;
	}

	public function add($column, array $values = [])
	{
		if ($column === NULL) {
			$this->collection = [];
			return $this;
		}

		$this->collection[] = $this->createStatement()->setColumn($column)->setValues($values);
		return $this;
	}

	public function isEmpty()
	{
		return !$this->collection;
	}

	public function createStatement()
	{
		return $this->statementFactory->createStatement();
	}

	public function append(Statement $statement)
	{
		if (!$statement->isEmpty()) {
			$this->collection[] = $statement;
		}
	}

	public function sql($delimiter = ', ')
	{
		return implode($delimiter, $this->collection);
	}

	public function getQuery()
	{

	}

}
