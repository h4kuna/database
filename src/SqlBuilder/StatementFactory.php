<?php

namespace h4kuna\Database\SqlBuilder;

use h4kuna\Database\Storage\Driver;

class StatementFactory
{

	/** @var Driver\GrammarInterface */
	private $grammar;

	public function __construct(Driver\GrammarInterface $grammar)
	{
		$this->grammar = $grammar;
	}

	public function createStatement()
	{
		return new Statement($this->grammar);
	}

	public function createStatementCollection()
	{
		return new StatementCollection($this);
	}

	public function createCondition(SUIDAbstract $command)
	{
		return new Condition($command, $this->createStatementCollection());
	}

}
