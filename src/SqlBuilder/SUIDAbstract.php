<?php

namespace h4kuna\Database\SqlBuilder;

use h4kuna\Database\Result,
	h4kuna\Database\Storage\Driver;

abstract class SUIDAbstract implements CommandInterface, \IteratorAggregate
{

	private $counter = 0x10000; // 2^16

	/** @var TableCollection */
	private $tableCollection;

	/** @var Driver\GrammarInterface */
	private $grammar;

	/** @var Driver\QueryInterface */
	private $query;

	/** @var StatementFactory */
	private $statementFactory;

	public function __construct(Table $table, Driver\GrammarInterface $grammar, TableCollection $tableCollection, Driver\QueryInterface $query, StatementFactory $statementFactory)
	{
		$this->statementFactory = $statementFactory;
		$this->grammar = $grammar;
		$this->query = $query;
		$this->tableCollection = $tableCollection;
		$this->setTable($table);
	}

	public function getCounter()
	{
		return --$this->counter;
	}

	/**
	 * @param string $key
	 * @param string $value
	 * @return Result\Row[]|array
	 */
	public function fetchAll($key = NULL, $value = NULL)
	{
		return $this->execute()->fetchAll($key, $value);
	}

	/** @return Result\RowCollection */
	public function execute()
	{
		return $this->query->execute($this);
	}

	/** @return mixed */
	public function fetchSingle()
	{
		return $this->execute()->fetchSingle();
	}

	/** @return Result\Row */
	public function fetch()
	{
		return $this->execute()->fetch();
	}

	/** @return Driver\GrammarInterface */
	protected function getGrammar()
	{
		return $this->grammar;
	}

	protected function getTableCollection()
	{
		return $this->tableCollection;
	}

	protected function createStatementCollection()
	{
		return $this->statementFactory->createStatementCollection();
	}

	protected function getStatementFactory()
	{
		return $this->statementFactory;
	}

	public function getIterator()
	{
		return $this->execute();
	}

	abstract protected function setTable(Table $table);
}
