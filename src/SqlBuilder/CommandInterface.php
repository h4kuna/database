<?php

namespace h4kuna\Database\SqlBuilder;

interface CommandInterface
{

	/** @return string */
	function sql();

	/** @var Query */
	function getQuery();
}
