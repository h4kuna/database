<?php

namespace h4kuna\Database\SqlBuilder;

/**
 * @deprecated use insert only
 */
class Query
{

	/** @var string */
	private $sql;

	/** @var array */
	private $params;

	/** @var array */
	private $columns;

	public function __construct($sql, $columns, $params)
	{
		$this->sql = $sql;
		$this->columns = $columns;
		$this->params = $params;
	}

	public function getSql()
	{
		return $this->sql;
	}

	public function getParams()
	{
		return $this->params;
	}

	public function sql()
	{
		return str_replace($this->columns, $this->params, $this->sql);
	}

}
