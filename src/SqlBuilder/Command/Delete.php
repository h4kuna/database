<?php

namespace h4kuna\Database\SqlBuilder\Command;

use h4kuna\Database;

class Delete extends Database\SqlBuilder\SUIDAbstract
{

	use Sub\FromTrait;
	use Sub\WhereTrait;
	use Sub\ReturningTrait;

	/** @var bool */
	private $force = FALSE;

	/** @var SqlBuilder\Table[] */
	private $using = [];

	public function force()
	{
		$this->force = TRUE;
		return $this;
	}

	/**
	 * @param string|Database\SqlBuilder\Table $name
	 * @param string $newAlias
	 * @return self
	 */
	public function using($name, $newAlias = NULL)
	{
		if ($name === NULL) {
			$this->using = [];
			return $this;
		}

		$table = $this->getTableCollection()->createNewTempAlias($name, $newAlias);
		$this->using[] = $table;
		return $this;
	}

	public function getQuery()
	{

	}

	public function sql()
	{
		$where = $this->whereSql();
		if (!$where && !$this->force) {
			throw new Database\DangerousQueryException('DELETE: Call method force(), if you know what you do.');
		}

		$sql = 'DELETE' . $this->fromSql();

		$using = '';
		foreach ($this->using as $i => $table) {
			if ($i) {
				$using .=', ';
			} else {
				$using .= ' USING ';
			}
			$using .= $table->sql();
		}

		return $sql . $using . $where . $this->returningSql() . ';';
	}

	protected function setTable(Database\SqlBuilder\Table $table)
	{
		$this->from($table);
	}

}
