<?php

namespace h4kuna\Database\SqlBuilder\Command;

use h4kuna\Database;

class Update extends Database\SqlBuilder\SUIDAbstract
{

	use Sub\FromTrait,
	 Sub\WhereTrait,
	 Sub\ReturningTrait;

	/** @var array */
	private $data;

	/** @var bool */
	private $force = FALSE;

	/** @var Database\SqlBuilder\Table */
	private $table;

	/**
	 * @param array $data
	 * @return self
	 * @throws Database\InvalidArrayException
	 */
	public function setData($data)
	{
		if ($data === NULL) {
			$this->data = [];
			return $this;
		} elseif (!Database\SqlBuilder\Utils::isArray($data)) {
			throw new Database\InvalidArrayException('UPDATE: Non-array data.');
		}
		$this->data = (array) $data;
		return $this;
	}

	public function force()
	{
		$this->force = TRUE;
		return $this;
	}

	public function getQuery()
	{

	}

	public function sql()
	{
		$where = $this->whereSql();

		if (!$where && !$this->force) {
			throw new Database\DangerousQueryException('UPDATE: Call method force(), if you know what you do.');
		} elseif (!$this->data) {
			throw new Database\InvalidArgumentException('Data for update are missing.');
		}

		$grammar = $this->getGrammar();
		$sql = 'UPDATE ' . $this->table->sql() . ' SET';

		foreach ($this->data as $column => $value) {
			$sql .= ' ' . $grammar->queryColumn($column) . '=' . $grammar->queryValue($value, TRUE);
		}
		$sql .= $this->fromSql(FALSE) . $where;
		return $sql . $this->returningSql() . ';';
	}

	protected function setTable(Database\SqlBuilder\Table $table)
	{
		$this->table = $table;
	}

}
