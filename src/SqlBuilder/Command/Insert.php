<?php

namespace h4kuna\Database\SqlBuilder\Command;

use h4kuna\Database;

class Insert extends Database\SqlBuilder\SUIDAbstract
{

	use Sub\ReturningTrait;

	/** @var array */
	private $rows;

	/** @var Query */
	private $query;

	/** @var Database\SqlBuilder\Table */
	private $table;

	/**
	 * @param array $data
	 * @return self
	 * @throws Database\InvalidArrayException
	 */
	public function addRow($data)
	{
		if (!Database\SqlBuilder\Utils::isArray($data)) {
			throw new Database\InvalidArrayException;
		}
		$this->rows[] = (array) $data;
		return $this;
	}

	public function sql()
	{
		return $this->createSql($this->rows);
	}

	public function getQuery()
	{
		if ($this->query) {
			return $this->query;
		}
		$rows = $values = $columns = [];
		foreach ($this->rows as $row) {
			$data = [];
			foreach ($row as $field => $value) {
				$columns[] = $data[$field] = $this->getGrammar()->markParameter($this->getCounter());
				$values[] = $value;
			}
			$rows[] = $data;
		}
		return $this->query = new Database\SqlBuilder\Query($this->createSql($rows), $columns, $values);
	}

	private function createSql($rows)
	{
		if ($this->query) {
			return $this->query->sql();
		}
		$columns = array_keys(current($rows));

		$sql = 'INSERT INTO ' . $this->table->sql(FALSE);
		$sql .= ' (' . $this->getGrammar()->prepareColumns($columns) . ') VALUES';

		$i = 0;
		foreach ($rows as $row) {

			if ($i > 0) {
				$sql .= ',';
			}
			$sql .= ' ' . $this->getGrammar()->prepareValuesSave($row);
			++$i;
		}

		return $sql . $this->returningSql() . ';';
	}

	protected function setTable(Database\SqlBuilder\Table $table)
	{
		$this->table = $table;
	}

}
