<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

trait WhereTrait
{

	/** @var Condition */
	private $where;

	/**
	 * @param string $column
	 * @param mixed $value
	 * @return Condition
	 */
	public function where($column, $value = NULL)
	{
		if ($this->where === NULL) {
			$this->where = $this->getStatementFactory()->createCondition($this);
		}
		return call_user_func_array([$this->where, 'andIf'], func_get_args());
	}

	protected function whereSql()
	{
		if (!$this->where || $this->where->isEmpty()) {
			return '';
		}

		return ' WHERE' . $this->where->sql(FALSE);
	}

}
