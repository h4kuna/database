<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database\SqlBuilder;

trait GroupTrait
{

	/** @var StatementCollection */
	private $groups;

	/**
	 * @param string $statement
	 * @param string|int|float $values
	 * @return this
	 */
	public function group($statement, $values = NULL /* ... */)
	{
		if ($this->groups === NULL) {
			$this->groups = $this->createStatementCollection();
		}
		call_user_func_array([$this->groups, 'add'], SqlBuilder\Utils::prepareArguments(func_get_args()));
		return $this;
	}

	protected function groupSql()
	{
		if (!$this->groups || $this->groups->isEmpty()) {
			return '';
		}
		return ' GROUP BY' . $this->groups->sql();
	}

	/** @return StatementCollection */
	// abstract protected function createStatementCollection();
}
