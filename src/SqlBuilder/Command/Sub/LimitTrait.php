<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

trait LimitTrait
{

	/** @var int|NULL */
	private $limit;

	/** @var int|NULL */
	private $offset;

	public function limit($limit, $offset = 0)
	{
		if ($limit === NULL) {
			$this->limit = $this->offset = NULL;
			return $this;
		}
		$this->limit = $limit;
		$this->offset($offset);
		return $this;
	}

	public function offset($offset)
	{
		$this->offset = $offset;
		return $this;
	}

	public function limitPage($page, $itemPerPage)
	{
		$offset = 0;
		if ($page > 1) {
			$offset = ($page - 1) * $itemPerPage;
		}
		return $this->limit($itemPerPage, $offset);
	}

	protected function limitSql()
	{
		$sql = '';
		if ($this->limit > 0) {
			$sql .= ' LIMIT ' . intval($this->limit);
		}

		if ($this->offset > 0) {
			$sql .= ' OFFSET ' . intval($this->offset);
		}

		return $sql;
	}

}
