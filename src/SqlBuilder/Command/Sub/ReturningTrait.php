<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database\SqlBuilder;

trait ReturningTrait
{

	/** @var StatementCollection */
	private $returning;

	/**
	 * @param string|array $column
	 * @return self
	 */
	public function returning($column = '*', $values = NULL /* ... */)
	{
		if ($this->returning === NULL) {
			$this->returning = $this->createStatementCollection();
		}

		call_user_func_array([$this->returning, 'add'], SqlBuilder\Utils::prepareArguments(func_get_args(), ['*']));
		return $this;
	}

	protected function returningSql()
	{
		if (!$this->returning || $this->returning->isEmpty()) {
			return '';
		}

		return ' RETURNING' . $this->returning->sql(',');
	}

	/** @return StatementCollection */
	// abstract protected function createStatementCollection();
}
