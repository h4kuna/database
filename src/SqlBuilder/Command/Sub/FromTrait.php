<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database,
	h4kuna\Database\SqlBuilder;

trait FromTrait
{

	/**
	 * @var SqlBuilder\Table[]
	 */
	private $tables = [];

	/**
	 * @param string|SqlBuilder\Table $name
	 * @return this
	 */
	public function from($name, $newAlias = NULL)
	{
		if ($name === NULL) {
			$this->tables = [];
			return $this;
		}

		$this->tables[] = $this->getTableCollection()->createNewTempAlias($name, $newAlias);
		return $this;
	}

	protected function fromSql($showAlias = TRUE)
	{
		if (!$this->tables) {
			return '';
		}
		$sql = ' FROM ';

		foreach ($this->tables as $key => $table) {
			if ($key) {
				$sql .= ', ';
			}
			$sql .= $table->sql($showAlias);
		}

		return $sql;
	}

	/** @return SqlBuilder\TableCollection  */
	// abstract protected function getTableCollection();

	/** @return \h4kuna\Database\Storage\Driver\GrammarInterface */
	// abstract protected function getGrammar();
}
