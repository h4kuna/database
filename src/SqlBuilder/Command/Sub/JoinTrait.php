<?php

namespace h4kuna\Database\SqlBuilder\Command\Sub;

use h4kuna\Database,
	h4kuna\Database\SqlBuilder;

trait JoinTrait
{

	/** @var SqlBuilder\Table[] */
	private $join = [];

	/** @var Condition */
	private $lastJoin;

	/**
	 * @param string|SqlBuilder\Table $name
	 * @param string|NULL $newAlias
	 * @return \this
	 */
	public function join($name, $newAlias = NULL)
	{
		return $this->createJoin($name, $newAlias, '');
	}

	/**
	 * @param string|SqlBuilder\Table $name
	 * @param string $newAlias
	 * @return \this
	 */
	public function leftJoin($name, $newAlias = NULL)
	{
		return $this->createJoin($name, $newAlias, 'LEFT');
	}

	/**
	 * @param string|SqlBuilder\Table $name
	 * @param string $newAlias
	 * @param string $joinType
	 * @return \this
	 */
	public function createJoin($name, $newAlias = NULL, $joinType = 'RIGHT')
	{
		$this->lastJoin = $this->getStatementFactory()->createCondition($this);

		$table = $this->getTableCollection()->createNewTempAlias($name, $newAlias);

		$this->join[$table->getAlias()] = [
			'condition' => $this->lastJoin,
			'table' => $table,
			'type' => $joinType ? (' ' . $joinType) : ''
		];

		return $this;
	}

	/**
	 * @param string $column
	 * @param mixed $value *
	 * @return Condition
	 */
	public function on($column, $value = NULL /* ... */)
	{
		if (!$this->lastJoin) {
			throw new Database\InvalidStateException('Call method join(), joinLeft() before.');
		}
		return call_user_func_array([$this->lastJoin, 'andIf'], func_get_args());
	}

	protected function joinSql()
	{
		$sql = '';
		foreach ($this->join as $data) {
			$sql .= $data['type'] . ' JOIN ' . $data['table']->sql() . ' ON' . $data['condition']->sql(FALSE);
		}

		return $sql;
	}

	/** @return SqlBuilder\TableCollection  */
	// abstract protected function getTableCollection();

}
