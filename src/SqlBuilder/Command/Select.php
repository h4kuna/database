<?php

namespace h4kuna\Database\SqlBuilder\Command;

use h4kuna\Database,
	h4kuna\Database\SqlBuilder;

class Select extends Database\SqlBuilder\SUIDAbstract
{

	use Sub\FromTrait,
	 Sub\JoinTrait,
	 Sub\WhereTrait,
	 Sub\GroupTrait,
	 Sub\OrderTrait,
	 Sub\LimitTrait;

	/** @var bool */
	private $distinct = FALSE;

	/** @var Sub\StatementCollection */
	private $columns;

	/** @var Sub\StatementCollection */
	private $window;

	/** @var Select[] */
	private $set = [];

	/** @var string */
	private $alias;

	/** @var Sub\Condition */
	private $having;

	/** @var Sub\Statement */
	private $lock;

	public function getAlias()
	{
		return $this->alias;
	}

	public function setSubSelect($alias = TRUE)
	{
		$this->alias = $alias;
		return $this;
	}

	public function distinct($expression = '')
	{
		if ($expression === NULL) {
			$this->distinct = NULL;
			return $this;
		}

		$this->distinct = ' DISTINCT';
		if ($expression) {
			$this->distinct .= ' ON (' . $this->getGrammar()->queryColumn($expression) . ')';
		}

		return $this;
	}

	/**
	 * @param string $column
	 * @param mixed $values *
	 * @return self
	 */
	public function column($column, $values = NULL /* ... */)
	{
		if ($this->columns === NULL) {
			$this->columns = $this->createStatementCollection();
		}

		call_user_func_array([$this->columns, 'add'], SqlBuilder\Utils::prepareArguments(func_get_args()));
		return $this;
	}

	/**
	 * @param string $column
	 * @param mixed $values *
	 * @return Sub\Condition
	 */
	public function having($column, $values = NULL /* ... */)
	{
		if ($this->having === NULL) {
			$this->having = $this->getStatementFactory()->createCondition($this);
		}

		return call_user_func_array([$this->having, 'andIf'], func_get_args());
	}

	public function union(self $select)
	{
		return $this->appendSet($select, 'UNION');
	}

	public function unionAll(self $select)
	{
		return $this->appendSet($select, 'UNION', TRUE);
	}

	public function appendSet(self $select, $type = 'EXCEPT', $all = FALSE)
	{
		if ($all) {
			$type .=' ALL';
		}

		$this->set[] = [
			'type' => ' ' . $type,
			'select' => $select
		];
		return $this;
	}

	public function intersect(self $select)
	{
		return $this->appendSet($select, 'INTERSECT');
	}

	public function intersectAll(self $select)
	{
		return $this->appendSet($select, 'INTERSECT', TRUE);
	}

	/**
	 * @param string $column
	 * @param mixed $values
	 * @return self
	 */
	public function forLock($column, $values = NULL /* ... */)
	{
		if ($column === NULL) {
			$this->lock = NULL;
			return $this;
		}
		$this->lock = $this->getStatementFactory()->createStatement();
		$this->lock->setColumn('FOR ' . $column)
			->setValues(array_slice(func_get_args(), 1));
		return $this;
	}

	public function window($name, $values = NULL)
	{
		if ($this->window === NULL) {
			$this->window = $this->createStatementCollection();
		}
		call_user_func_array([$this->window, 'add'], SqlBuilder\Utils::prepareArguments(func_get_args()));
		return $this;
	}

	public function count()
	{
		$copy = clone $this;
		$copy->window = $copy->limit = $copy->orders = $copy->columns = $copy->lock = $copy->having = $copy->offset = NULL;
		return intval($copy->column('COUNT(*)')->fetchSingle());
	}

	public function getQuery()
	{

	}

	private function havingSql()
	{
		if (!$this->having || $this->having->isEmpty()) {
			return '';
		}
		return ' HAVING' . $this->having->sql(FALSE);
	}

	/**
	 * @todo cache doubre rendereing move to parent abstract class?
	 * @return string
	 */
	public function sql()
	{
		$sql = 'SELECT' . $this->distinct;
		if (!$this->columns || $this->columns->isEmpty()) {
			$sql .= ' *';
		} else {
			$sql .= $this->columns->sql();
		}

		$sql .= $this->fromSql();
		$sql .= $this->joinSql();
		$sql .= $this->whereSql();
		$sql .= $this->groupSql();
		$sql .= $this->havingSql();
		$sql .= $this->windowSql();
		$sql .= $this->setSql();
		$sql .= $this->orderSql();
		$sql .= $this->limitSql();
		if ($this->lock) {
			$sql .= $this->lock->sql();
		}
		return $sql . ($this->alias ? NULL : ';');
	}

	private function setSql()
	{
		$sql = '';
		foreach ($this->set as $data) {
			$sql .= $data['type'] . ' (' . $data['select']->setSubSelect()->sql() . ')';
		}
		return $sql;
	}

	private function windowSql()
	{
		if (!$this->window || $this->window->isEmpty()) {
			return '';
		}
		return ' WINDOW' . $this->window->sql(',');
	}

	protected function setTable(Database\SqlBuilder\Table $table)
	{
		if ($table->getTable() instanceof self) {
			$table->getTable()->alias = $table->getAlias();
		}
		$this->from($table);
	}

	public function __toString()
	{
		return $this->sql();
	}

}
