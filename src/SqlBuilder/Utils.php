<?php

namespace h4kuna\Database\SqlBuilder;

class Utils
{

	public static function isArray($data)
	{
		return is_array($data) || $data instanceof \ArrayAccess;
	}

	public static function prepareArguments(array $allArguments, $default = [])
	{
		if ($allArguments === []) {
			return $default;
		}
		$column = [array_shift($allArguments)];
		if ($allArguments) {
			array_push($column, $allArguments);
		}
		return $column;
	}

}
