<?php

namespace h4kuna\Database\Storage;

use h4kuna\Database;

class SqlLog
{

	/** @var callable */
	public $onExplain;

	/** @var string last send sql */
	public static $sql;

	/** @var bool */
	private $development;

	/** @var array */
	private $listSql = [];

	/** @var array */
	private $last;

	/** @var string */
	private $explain = 'EXPLAIN ?;';

	/** @var float */
	private $tempTime;

	public function __construct($development)
	{
		$this->development = (bool) $development;
	}

	public function log($sql)
	{
		if (!$this->development) {
			return $this;
		}

		self::$sql = $sql;

		$this->last = [
			'sql' => $sql,
			'time' => 0, // ms
			'explain' => ''
		];

		$this->listSql[] = & $this->last;
		return $this;
	}

	public function setExplain($sql)
	{
		$this->explain = $sql;
	}

	public function start()
	{
		if (!$this->development) {
			return NULL;
		}
		$this->tempTime = microtime(TRUE);
	}

	public function end()
	{
		if (!$this->development) {
			return NULL;
		}
		$this->last['time'] = microtime(TRUE) - $this->tempTime;
		$this->tempTime = 0;
	}

	public function explain()
	{
		if (!$this->development || !$this->explain) {
			return NULL;
		}

		if (!$this->onExplain) {
			throw new Database\InvalidStateException('Let\'s define callback how prepare explained data.');
		}
		$this->development = FALSE;
		$this->last['explain'] = call_user_func($this->onExplain, str_replace('?', rtrim($this->last['sql'], ';'), $this->explain));
		$this->development = TRUE;
	}

	public function getListSql()
	{
		return $this->listSql;
	}

}
