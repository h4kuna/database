<?php

namespace h4kuna\Database\Storage;

/**
 * Support PHP 5.4+
 */
class PostgreSql extends ContextAbstract
{

	/** @var Poll */
	private $poll;

	private function createConnection()
	{
		if ($this->connection) {
			return $this->connection;
		}
		$this->connection = $connection;
		if ($this->connection->isAsync()) {
			$this->poll = new Driver\PostgreSql\Poll($this->connection);
		}
//		$preprocesor = new Preprocessor();
//		$this->query = new Query($this->connection, $preprocesor);
	}

	protected function createTransaction()
	{
		return new Driver\PostgreSql\Transaction($this->getConnection(), $this->getQuery());
	}

	protected function createGrammar()
	{
		return new Driver\PostgreSql\Grammar();
	}

	protected function createQuery()
	{
		return new Driver\PostgreSql\Query($this->getConnection(), $this->getStatementFactory(), $this->getRowCollectionFactory(), $this->getSqlLog());
	}

	protected function createTableMetadata()
	{
		return new Driver\PostgreSql\TableMetadata($this->getConnection());
	}

	protected function getExplainResult(Driver\QueryInterface $query)
	{
		return function($sql) use ($query) {
			$result = $query->noExplainExecute($sql);
			// prepare result for any debug bar
			$data = pg_fetch_all($result);
			pg_free_result($result);
			return $data;
		};
	}

}
