<?php

namespace h4kuna\Database\Storage\Driver;

interface InformationInterface
{

	/** @return int */
	function getPort();

	/** @return string */
	function getDbname();

	/** @return string */
	function getClientEncoding();

	/** @return string */
	function getDbVersion();

	/** @return array */
	function getOptions();

	/** @return mixed */
	function getParameter($parameter);
}
