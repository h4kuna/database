<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use h4kuna\Database\Storage\Driver;

/**
 * Support PHP 5.6
 */
class Poll
{

	/** @var Connection */
	private $connection;

	public function __construct(Connection $connection)
	{
		if (PHP_VERSION_ID < 50600) {
			throw new Driver\LowPhpVersionException('5.6+');
		}
		$this->connection = $connection;
	}

	private function polling()
	{
		return pg_connect_poll($this->connection->getConnection());
	}

	public function isReadable()
	{
		$read = [$this->connection->getSocket()];
		$write = $ex = [];
		return (bool) stream_select($read, $write, $ex, $usec = 1, 0);
	}

	public function isWriteable()
	{
		$write = [$this->connection->getSocket()];
		$read = $ex = [];
		return (bool) stream_select($read, $write, $ex, $usec = 1, 0);
	}

}
