<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use h4kuna\Database\Storage\Driver;

class Information implements Driver\InformationInterface
{

	/** @var Connection */
	private $connection;

	/** @var int */
	private $serverVersion;

	public function __construct(Connection $connection)
	{
		$this->connection = $connection;
	}

	public function getPort()
	{
		return pg_port($this->connection->getConnection());
	}

	public function getDbname()
	{
		return pg_dbname($this->connection->getConnection());
	}

	public function getClientEncoding()
	{
		return pg_client_encoding($this->connection->getConnection());
	}

	public function getDbVersion()
	{
		if ($this->serverVersion) {
			return $this->serverVersion;
		}

		$data = pg_version($this->connection->getConnection());
		$out = '';
		foreach (explode('.', $data['server']) as $number) {
			$out .= str_pad($number, 2, '0', STR_PAD_LEFT);
		}
		return $this->serverVersion = intval($out);
	}

	public function getOptions()
	{
		return pg_options($this->connection->getConnection());
	}

	public function getParameter($parameter)
	{
		return pg_parameter_status($this->connection->getConnection(), $parameter);
	}

}
