<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use h4kuna\Database,
	h4kuna\Database\Result,
	h4kuna\Database\SqlBuilder;

class Query implements Database\Storage\Driver\QueryInterface
{

	/** @var Connection */
	private $connection;

	/** @var SqlBuilder\StatementFactory */
	private $statementFactory;

	/** @var Database\Response\ResultFactory */
	private $rowCollectionFactory;

	/** @var Database\Storage\SqlLog */
	private $sqlLog;

	public function __construct(Connection $connection, SqlBuilder\StatementFactory $statementFactory, Result\RowCollectionFactory $rowCollectionFactory, Database\Storage\SqlLog $sqlLog)
	{
		$this->connection = $connection;
		$this->statementFactory = $statementFactory;
		$this->rowCollectionFactory = $rowCollectionFactory;
		$this->sqlLog = $sqlLog;
	}

	public function execute($sql, $values = NULL /* ... */)
	{
		if (is_string($sql)) {
			if (func_num_args() > 1) {
				$statement = $this->statementFactory->createStatement();
				$args = func_get_args();
				array_shift($args);
				$statement->setColumn($sql)->setValues($args);
				return $this->run((string) $statement);
			}
			return $this->run($sql);
		} elseif ($sql instanceof SqlBuilder\SUIDAbstract) {
			return $this->run($sql->sql());
		}
		throw new Database\InvalidArgumentException('Parameter must be string or SUIDAbstract object.');
	}

	private function run($sql)
	{
		return $this->rowCollectionFactory->create(new Source($this->runSql($sql)));
	}

	public function noExplainExecute($sql)
	{
		$this->sqlLog->log($sql)->start();
		$result = @pg_query($this->connection->getConnection(), $sql);
		$this->sqlLog->end();
		if ($result === FALSE) {
			throw $this->throwException();
		}
		return $result;
	}

	/**
	 * @param string $sql
	 * @return resource
	 */
	private function runSql($sql)
	{
		$result = $this->noExplainExecute($sql);
		$this->sqlLog->explain();
		return $result;
	}

	/** @return Database\DatabaseResponse */
	private function throwException()
	{
		$error = $this->connection->getLastError();
		// $version = $this->connection->getInformation()->getDbVersion();
		// test for $version  90120 and 90311
		if (preg_match('~relation "(.*)" already exists$~', $error, $find)) {
			return new Database\TableAlreadyExistsException($find[1]);
		} elseif (preg_match('~null value in column "(.*)" violates not-null constraint~U', $error, $find)) {
			return new Database\NotNullConstraintViolationException($find[1]);
		} elseif (preg_match('~Key \((?P<column>.*)\)=\((?P<value>.*)\) already exists~U', $error, $find)) {
			$exception = new Database\UniqueConstraintViolationException($error);

			$exception->init(self::explode($find['column']), self::explode($find['value']));
			return $exception;
		} elseif (preg_match('~Key \((?P<column>.*)\)=\((?P<value>.*)\) is still referenced from table~U', $error, $find)) {
			$exception = new Database\ForeingKeyContraintViolationException($error);

			$exception->init(self::explode($find['column']), self::explode($find['value']));
			return $exception;
		} elseif (preg_match('~Key \((?P<column>.*)\)=\((?P<value>.*)\) is not present in table~U', $error, $find)) {
			$exception = new Database\ForeingKeyContraintViolationException($error);

			$exception->init(self::explode($find['column']), self::explode($find['value']));
			return $exception;
		}

		return new Database\QueryException($error);
	}

	private static function explode($string)
	{
		return array_map(function($value) {
			return trim($value, '"');
		}, explode(', ', $string));
	}

	// pg_cancel_query
	// pg_copy_from
	// pg_copy_to
	// pg_put_line
	// pg_delete
	// pg_update
	// pg_execute
	// pg_query
	// pg_select
	// pg_send_execute
}
