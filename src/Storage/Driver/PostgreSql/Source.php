<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use h4kuna\Database,
	h4kuna\Database\Storage\Driver,
	h4kuna\Database\Reflection;

class Source implements Driver\SourceInterface
{

	/** @var Reflection\ReflectionInterface */
	private $reflection;

	/** @var Driver\GrammarInterface */
	private $grammar;

	/** @var resource */
	private $resource;

	/** @var mixed */
	private $rows;

	public function __construct($resource)
	{
		$this->resource = $resource;
	}

	public function setReflection(Reflection\ReflectionInterface $resultConfig)
	{
		$this->reflection = $resultConfig;
	}

	public function setGrammar(Driver\GrammarInterface $grammar)
	{
		$this->grammar = $grammar;
	}

	public function current()
	{
		return current($this->rows);
	}

	public function key()
	{
		return key($this->rows);
	}

	public function next()
	{
		next($this->rows);
	}

	public function rewind()
	{
		$this->bindRows();
		reset($this->rows);
	}

	public function valid()
	{
		return isset($this->rows[$this->key()]);
	}

	private function bindRows()
	{
		if ($this->rows !== NULL) {
			return NULL;
		}

		$this->rows = $columnTypes = [];
		$rows = pg_fetch_all($this->resource);

		if (!$rows) {
			return NULL;
		}

		$countColumns = pg_num_fields($this->resource);
		$firstRow = reset($rows);
		if (count($firstRow) !== $countColumns) {
			throw new Database\DumplicityColumnNameException('Any of (' . implode(', ', array_keys($firstRow)) . ') is duplicity, fix it.');
		}
		foreach ($firstRow as $name => $value) {
			$columnTypes[$name] = $this->grammar->normalizeType(pg_field_type($this->resource, pg_field_num($this->resource, $name)));
		}

		foreach ($rows as $data) {
			$this->rows[] = $this->reflection->createRow($data, $columnTypes);
		}
		pg_free_result($this->resource);
	}

	public function getRows()
	{
		$this->bindRows();
		return $this->rows;
	}

	public function count()
	{
		$this->bindRows();
		return count($this->rows);
	}

}
