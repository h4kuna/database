<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use h4kuna\Database,
	h4kuna\Database\Storage\Driver;

class Transaction implements Driver\TransactionInterface
{
	// pg_transaction_status

	/** @var Connection */
	private $connection;

	/** @var Query */
	private $query;

	/** @var int 0 - mean not in transaction */
	private $id = 0;

	public function __construct(Connection $connection, Query $query)
	{
		if (PHP_VERSION_ID < 50100) {
			throw new Driver\LowPhpVersionException('5.1+');
		}
		$this->connection = $connection;
		$this->query = $query;
	}

	public function begin()
	{
		if ($this->inTransaction()) {
			++$this->id;
			$this->query->noExplainExecute('SAVEPOINT ' . $this->getSavepoint() . ';');
		} else {
			++$this->id;
			$this->query->noExplainExecute('BEGIN;');
		}
		return $this->id;
	}

	public function commit()
	{
		if ($this->checkTransaction() > 1) {
			$this->query->noExplainExecute('RELEASE SAVEPOINT ' . $this->getSavepoint() . ';');
			--$this->id;
		} else {
			$this->query->noExplainExecute('COMMIT;');
			$this->id = 0;
		}
		return $this->id;
	}

	public function rollback()
	{
		if ($this->checkTransaction() > 1) {
			$this->query->noExplainExecute('ROLLBACK TO SAVEPOINT ' . $this->getSavepoint() . ';');
			--$this->id;
		} else {
			$this->query->noExplainExecute('ROLLBACK;');
			$this->id = 0;
		}
		return $this->id;
	}

	public function inTransaction()
	{
		return in_array($this->getStatus(), [PGSQL_TRANSACTION_ACTIVE, PGSQL_TRANSACTION_INTRANS]);
	}

	private function getStatus()
	{
		return pg_transaction_status($this->connection->getConnection());
	}

	private function getSavepoint()
	{
		return 'id_' . $this->id;
	}

	/** @return int */
	private function checkTransaction()
	{
		if (!$this->inTransaction()) {
			throw new Database\InvalidStateException('Let\'s start transaction via begin().');
		}
		return $this->id;
	}

}
