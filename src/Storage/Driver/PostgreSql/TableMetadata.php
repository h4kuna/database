<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use h4kuna\Database\Storage\Driver,
	h4kuna\Database\SqlBuilder;

class TableMetadata implements Driver\TableMetadataInterface
{

	/** @var array */
	private $metadata = [];

	/** @var Connection */
	private $connection;

	/** @var bool  */
	private $extended = FALSE;

	public function __construct(Connection $connection)
	{
		$this->connection = $connection;
	}

	public function needMoreInfo()
	{
		$this->extended = TRUE;
	}

	public function getMetadata($table)
	{
		if ($table instanceof SqlBuilder\Table) {
			$table = $table->getTable();
		}
		if (isset($this->metadata[$table])) {
			return $this->metadata[$table];
		}
		if (PHP_VERSION_ID < 50600) {
			$data = pg_meta_data($this->connection->getConnection(), $table);
		} else {
			$data = pg_meta_data($this->connection->getConnection(), $table, $this->extended);
		}
		return $this->metadata[$table] = $data;
	}

}
