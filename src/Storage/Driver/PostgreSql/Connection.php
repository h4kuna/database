<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use h4kuna\Database,
	h4kuna\Database\Storage\Driver;

foreach ([
 'PGSQL_CONNECTION_STARTED' => 2,
 'PGSQL_CONNECTION_MADE' => 3,
 'PGSQL_CONNECTION_AWAITING_RESPONSE' => 4,
 'PGSQL_CONNECTION_AUTH_OK' => 5,
 'PGSQL_CONNECTION_SETENV' => 6,
 'PGSQL_CONNECTION_SSL_STARTUP' => 7
] as $define => $value) {
	if (!defined($define)) {
		define($define, $value);
	}
}

class Connection implements Driver\ConnectionInterface
{

	const AUTO_CLOSE = 8;

	private $socket;
	public static $connStatus = [
		PGSQL_CONNECTION_OK => "CONNECTION_OK",
		PGSQL_CONNECTION_BAD => "CONNECTION_BAD",
		PGSQL_CONNECTION_STARTED => "CONNECTION_STARTED",
		PGSQL_CONNECTION_MADE => "CONNECTION_MADE",
		PGSQL_CONNECTION_AWAITING_RESPONSE => "CONNECTION_AWAITING_RESPONSE",
		PGSQL_CONNECTION_AUTH_OK => "CONNECTION_AUTH_OK",
		PGSQL_CONNECTION_SETENV => "CONNECTION_SETENV",
		PGSQL_CONNECTION_SSL_STARTUP => "CONNECTION_SSL_STARTUP",
	];
	private $setup = [
		'host' => 'localhost',
		'port' => 5432,
		'dbname' => NULL,
		'user' => NULL,
		'password' => NULL,
	];
	private $parameters; // PGSQL_CONNECT_ASYNC

	/** @var bool */
	private $config = self::AUTO_CLOSE;

	/** @var resource */
	private $connection;

	/** @var resource|string */
	private $trace;

	/** @var Driver\InformationInterface */
	private $information;

	public function __construct($user, $password, $dbname)
	{
		$this->setSetup([
			'user' => $user,
			'password' => $password,
			'dbname' => $dbname
		]);
	}

	public function copy($dbname = NULL, $user = NULL, $password = NULL)
	{
		$connection = clone $this;
		$connection->connection = NULL;
		$setup = [];
		foreach (['dbname', 'user', 'password'] as $param) {
			if ($$param) {
				$setup[$param] = $$param;
			}
		}
		if ($setup) {
			$connection->setSetup($setup);
		}
		return $connection;
	}

	public function disableAutoClose()
	{
		return $this->config -= self::AUTO_CLOSE;
	}

	public function setSetup(array $dns)
	{
		foreach ($this->setup as $k => $v) {
			if (!array_key_exists($k, $dns)) {
				continue;
			}
			$this->setup[$k] = $dns[$k];
		}
		return $this;
	}

	public function trace($pathname)
	{
		$this->trace = $pathname;
	}

	public function setParameters($parameters)
	{
		$this->parameters = intval($parameters);
		return $this;
	}

	public function socket()
	{
		return pg_socket($this->connection);
	}

	public function getSocket()
	{
		if ($this->socket === NULL) {
			$this->socket = $this->socket();
		}

		return $this->socket;
	}

	public function flush()
	{
		return pg_flush($this->getConnection());
	}

	public function getConnection()
	{
		if ($this->connection !== NULL) {
			return $this->connection;
		}

		$dns = '';
		foreach ($this->setup as $k => $v) {
			if ($v !== NULL) {
				$dns .= $k . '=' . $v . ' ';
			}
		}

		$this->connection = pg_connect($dns, $this->parameters);
		unset($dns);
		$this->setup = [];
		if (!$this->connection || $this->status() !== PGSQL_CONNECTION_OK) {
			throw new Database\ConnectionFailedException($this->getLastError());
		} elseif ($this->trace) {
			if (!@pg_trace($this->trace, 'w', $this->getConnection())) {
				throw new Driver\TraceConnectionFaildException('Check permission for path ' . $this->trace);
			}
			$this->trace = TRUE;
		}

		return $this->connection;
	}

	public function status()
	{
		return pg_connection_status($this->getConnection());
	}

	public function isBusy()
	{
		return pg_connection_busy($this->getConnection());
	}

	public function reset()
	{
		if ($this->connection === NULL) {
			return $this->getConnection();
		}
		if (pg_connection_reset($this->getConnection())) {
			throw new Driver\ConnectionFailedException($this->getLastError());
		}
		return $this->getConnection();
	}

	public function close()
	{
		if ($this->connection === NULL) {
			return NULL;
		}
		$this->utrace();
		pg_close($this->connection);
		$this->connection = $this->socket = NULL;
	}

	public function isAsync()
	{
		return ($this->parameters & PGSQL_CONNECT_ASYNC) === PGSQL_CONNECT_ASYNC;
	}

	public function getLastNotice()
	{
		if ($this->connection === NULL) {
			return NULL;
		}
		return pg_last_notice($this->connection);
	}

	public function getLastError()
	{
		if ($this->connection === NULL) {
			return NULL;
		}
		return pg_last_error($this->connection);
	}

	public function getConsumeInput()
	{
		if (PHP_VERSION_ID < 50600) {
			throw new Driver\LowPhpVersionException('5.6+');
		}
		return pg_consume_input($this->getConnection());
	}

	public function tty()
	{
		return pg_tty($this->getConnection());
	}

	public function ping()
	{
		return pg_ping($this->getConnection());
	}

	public function __destruct()
	{
		if ($this->config & self::AUTO_CLOSE) {
			$this->close();
		} else {
			$this->utrace();
		}
	}

	private function utrace()
	{
		if ($this->connection !== NULL && $this->trace === TRUE) {
			pg_untrace($this->connection);
		}
	}

	public function getInformation()
	{
		if ($this->information === NULL) {
			$this->information = new Information($this);
		}
		return $this->information;
	}

}
