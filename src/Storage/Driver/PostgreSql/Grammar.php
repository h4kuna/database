<?php

namespace h4kuna\Database\Storage\Driver\PostgreSql;

use h4kuna\Database,
	h4kuna\Database\Storage\Driver,
	h4kuna\Database\SqlBuilder,
	Nette\Utils;

class Grammar extends Driver\GrammerAbstract
{

	const DATE_FORMAT = 'Y-m-d H:i:s.uO';

	private $quote = "'";

	public function markParameter($parameter)
	{
		return '$' . $parameter;
	}

	public function queryColumn($column)
	{
		return $column;
	}

	public function queryTable(SqlBuilder\Table $table, $withAlias = TRUE)
	{
		$sql = $table->getTable();
		if ($withAlias && $table->getAlias()) {
			$sql .= ' AS ' . $table->getAlias();
		}
		return $sql;
	}

	public function queryValue($value, $save = FALSE)
	{
		if ($value instanceof SqlBuilder\Condition) {
			$value = $value->getCommand();
		}

		if (is_bool($value)) {
			return $this->quoteString($value === TRUE ? 't' : 'f');
		} elseif ($value instanceof SqlBuilder\Literal) {
			return $value->getCommand();
		} elseif (Utils\Validators::isNumericInt($value)) {
			return intval($value);
		} elseif (is_numeric($value)) {
			return floatval($value);
		} elseif (is_string($value)) {
			return $this->quoteString($value);
		} elseif ($value === NULL) {
			return 'NULL';
		} elseif ($value instanceof \DateTime) {
			return $this->quoteString($value->format(self::DATE_FORMAT));
		} elseif ($value instanceof \JsonSerializable) {
			return $this->quoteString($value->jsonSerialize());
		} elseif (is_array($value)) {
			if ($save === FALSE) {
				return $this->prepareValues($value);
			}
			$this->quote = '';
			$out = $this->toPgArray($value);
			$this->quote = "'";
			return $this->quote . $out . $this->quote;
		} elseif ($value instanceof SqlBuilder\Command\Select) {
			return $value->setSubSelect()->sql();
		}
		throw new Database\InvalidArgumentException;
	}

	private function toPgArray(array $set)
	{
		$result = [];
		foreach ($set as $item) {
			if (SqlBuilder\Utils::isArray($item)) {
				$result[] = self::toPgArray((array) $item);
			} else {
				$item = $this->queryValue($item, TRUE);
				if (is_string($item)) {
					$result[] = '"' . str_replace('"', '\\"', $item) . '"';
					continue;
				}
				$result[] = $item;
			}
		}
		return '{' . implode(', ', $result) . '}';
	}

	private function quoteString($value)
	{
		return $this->quote . pg_escape_string($value) . $this->quote;
	}

	private function toPhpArray($value, $type)
	{
		return array_map(function ($value) use ($type) {
			return $this->normalizeValue(trim($value, '"'), $type);
		}, explode(',', substr($value, 1, -1)));
	}

	public function normalizeValue($value, $type)
	{
		if ($type & self::TYPE_ARRAY) {
			$type -= self::TYPE_ARRAY;
			if ($type & self::TYPE_INT || $type & self::TYPE_FLOAT) {
				return eval('return ' . str_replace(['{', '}'], ['[', ']'], $value) . ';');
			}
			return self::toPhpArray($value, $type);
		}
		switch ($type) {
			case self::TYPE_BOOL:
				return $value === 't';
			case self::TYPE_FLOAT:
				return (float) $value;
			case self::TYPE_INT:
				return (int) $value;
			case self::TYPE_DATE:
				return self::toDatetime($value . ' 00:00:00.0');
			case self::TYPE_TIME:
				return self::toDatetime('1970-01-01 ' . $value);
			case self:: TYPE_DATETIME:
				return self::toDatetime($value);
			case self::TYPE_DATETIME_TZ:
				return date_create_from_format(self::DATE_FORMAT, $value);
		}

		return $value;
	}

	public function normalizeType($columnType)
	{
		if (substr($columnType, 0, 1) === '_') {
			return self::TYPE_ARRAY | self::normalizeType(substr($columnType, 1));
		}

		switch ($columnType) {
			case 'int':
			case 'int2':
			case 'int4':
			case 'int8':
			case 'serial2':
			case 'serial4':
			case 'serial8':
				return self::TYPE_INT;
			case 'varchar':
			case 'char':
				return self::TYPE_STRING;
			case 'float4':
			case 'float8':
				return self::TYPE_FLOAT;
			case 'bool':
				return self::TYPE_BOOL;
			case 'timestamp':
				return self::TYPE_DATETIME;
			case 'date':
				return self::TYPE_DATE;
			case 'timestamptz':
				return self::TYPE_DATETIME_TZ;
			case 'time':
				return self::TYPE_TIME;
		}
		return $columnType;
	}

	private static function toDatetime($value)
	{
		return date_create_from_format('Y-m-d H:i:s.u', $value);
	}

	// pg_escape_bytea
	// pg_escape_identifier
	// pg_escape_literal
	// pg_escape_string
	// pg_unescape_bytea
	// pg_prepare
	// pg_query_params
}
