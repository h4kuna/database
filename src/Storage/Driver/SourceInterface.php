<?php

namespace h4kuna\Database\Storage\Driver;

use h4kuna\Database\Reflection;

interface SourceInterface extends \Iterator, \Countable
{

	function __construct($resource);

	function setGrammar(GrammarInterface $grammar);

	function setReflection(Reflection\ReflectionInterface $reflection);

	function getRows();
}
