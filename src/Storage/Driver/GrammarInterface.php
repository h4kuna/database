<?php

namespace h4kuna\Database\Storage\Driver;

use h4kuna\Database\SqlBuilder;

interface GrammarInterface
{

	const
		TYPE_NULL = 1,
		TYPE_STRING = 2,
		TYPE_INT = 4,
		TYPE_FLOAT = 8,
		TYPE_BOOL = 16,
		TYPE_DATETIME = 32,
		TYPE_DATETIME_TZ = 64,
		TYPE_TIME = 128,
		TYPE_DATE = 256,
		TYPE_ARRAY = 1024;


	function markParameter($parameter);

	function queryValue($value, $save = FALSE);

	function queryColumn($column);

	function queryTable(SqlBuilder\Table $table, $showAlias = TRUE);

	function prepareValues(array & $data, $glue = ', ');

	function prepareValuesSave(array & $data, $glue = ', ');

	function prepareColumns(array & $data, $glue = ', ');

	function prepareColumnsAlias(array & $data, $glue = ', ');

	/**
	 * @param string $columnType
	 */
	function normalizeType($columnType);

	/**
	 * @param mixed $value
	 * @param int $type is value of self::TYPE_* constant
	 */
	function normalizeValue($value, $type);

}
