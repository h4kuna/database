<?php

namespace h4kuna\Database\Storage\Driver;

interface TransactionInterface
{
	function begin();

	function commit();

	function rollback();

	/** @return bool */
	function inTransaction();
}
