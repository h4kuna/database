<?php

namespace h4kuna\Database\Storage\Driver;

abstract class GrammerAbstract implements GrammarInterface
{

	public function prepareValues(array & $data, $glue = ', ')
	{
		return $this->prepareValue($data, $glue, FALSE);
	}

	public function prepareValuesSave(array & $data, $glue = ', ')
	{
		return $this->prepareValue($data, $glue, TRUE);
	}

	private function prepareValue(array & $data, $glue, $save)
	{
		$out = '';
		foreach ($data as $key => $column) {
			if ($out) {
				$out .= $glue;
			}
			$out .= $data[$key] = $this->queryValue($column, $save);
		}
		return '(' . $out . ')';
	}

	public function prepareColumns(array & $data, $glue = ', ')
	{
		return $this->prepareColumn($data, $glue, FALSE);
	}

	public function prepareColumnsAlias(array & $data, $glue = ', ')
	{
		return $this->prepareColumn($data, $glue, TRUE);
	}

	private function prepareColumn(array & $data, $glue, $alias)
	{
		$out = '';
		foreach ($data as $key => $column) {
			if ($out) {
				$out .= $glue;
			}
			$out .= $data[$key] = $this->queryColumn($column);
			if ($alias && !is_int($key)) {
				$out .= ' AS ' . $key; // @todo sanitize?
			}
		}
		return $out;
	}

}
