<?php

namespace h4kuna\Database\Storage\Driver;

use h4kuna\Database\Result,
	h4kuna\Database\SqlBuilder;

interface QueryInterface
{

	/**
	 * @param SqlBuilder\SUIDAbstract|string $command
	 * @param mixed $values
	 * @return Result\RowCollection
	 */
	function execute($command, $values = NULL /* ... */);

	/**
	 * Is only for transaction and debug.
	 * @internal
	 * @param string $sql
	 */
	function noExplainExecute($sql);
}
