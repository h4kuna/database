<?php

namespace h4kuna\Database\Storage\Driver;

interface ConnectionInterface
{

	function trace($pathname);

	/** @return InformationInterface */
	function getInformation();
}
