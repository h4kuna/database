<?php

namespace h4kuna\Database\Storage\Driver;

use h4kuna\Database\SqlBuilder;

interface TableMetadataInterface
{

	/**
	 * @param SqlBuilder\Table|string
	 * @return array
	 */
	function getMetadata($table);
}
