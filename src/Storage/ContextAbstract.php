<?php

namespace h4kuna\Database\Storage;

use h4kuna\Database\Reflection,
	h4kuna\Database\Result,
	h4kuna\Database\SqlBuilder;

abstract class ContextAbstract
{

	/** @var SqlBuilder\TableCollection */
	private $tableCollection;

	/** @vat Driver\TableMetadataInterface */
	private $tableMetadata;

	/** @var GrammarInterface */
	private $grammar;

	/** @var TransactionInterface  */
	private $transaction;

	/** @var SqlBuilder\StatementFactory */
	private $statementFactory;

	/** @var Driver\ConnectionInterface */
	private $connection;

	/** @var Driver\QueryInterface */
	private $query;

	/** @var Response\ResultFactory */
	private $resultFactory;

	/** @var SqlLog */
	private $sqlLog;

	/** @var bool */
	private $development;

	public function __construct(Driver\ConnectionInterface $connection, $development)
	{
		$this->development = $development;
		$this->connection = $connection;
	}

	/** @return SqlBuilder\TableCollection */
	public function getTableCollection()
	{
		if (!$this->tableCollection) {
			$this->tableCollection = new SqlBuilder\TableCollection($this->getGrammar(), $this->getQuery(), $this->getStatementFactory(), $this->getTableMetadata());
		}
		return $this->tableCollection;
	}

	public function getTableMetadata()
	{
		if (!$this->tableMetadata) {
			$this->tableMetadata = $this->createTableMetadata();
		}
		return $this->tableMetadata;
	}

	/** @return InformationInterface */
	public function getInformation()
	{
		return $this->connection->getInformation();
	}

	/** @return GrammarInterface */
	public function getGrammar()
	{
		if ($this->grammar === NULL) {
			$this->grammar = $this->createGrammar();
		}
		return $this->grammar;
	}

	/** @return TransactionInterface */
	public function getTransaction()
	{
		if ($this->transaction === NULL) {
			$this->transaction = $this->createTransaction();
		}
		return $this->transaction;
	}

	/** @return SqlBuilder\StatementFactory */
	public function getStatementFactory()
	{
		if ($this->statementFactory === NULL) {
			$this->statementFactory = new SqlBuilder\StatementFactory($this->getGrammar());
		}
		return $this->statementFactory;
	}

	/** @return Driver\QueryInterface */
	public function getQuery()
	{
		if ($this->query === NULL) {
			$this->query = $this->createQuery();
			if ($this->development) {
				$this->getSqlLog()->onExplain = $this->getExplainResult($this->query);
			}
		}

		return $this->query;
	}

	public function getSqlLog()
	{
		if ($this->sqlLog === NULL) {
			$this->sqlLog = $this->createSqlLog($this->development);
		}
		return $this->sqlLog;
	}

	/** @return Driver\ConnectionInterface */
	public function getConnection()
	{
		return $this->connection;
	}

	public function getRowCollectionFactory()
	{
		if ($this->resultFactory === NULL) {
			$this->resultFactory = $this->createRowCollectionFactory();
		}
		return $this->resultFactory;
	}

	protected function createRowCollectionFactory()
	{
		return new Result\RowCollectionFactory($this->createReflection(), $this->getGrammar());
	}

	protected function createReflection()
	{
		return new Reflection\DefaultReflection($this->getGrammar());
	}

	protected function createSqlLog($development)
	{
		return new SqlLog($development);
	}

	/** @return Driver\TransactionInterface */
	abstract protected function createTransaction();

	/** @return Driver\GrammarInterface */
	abstract protected function createGrammar();

	/** @return Driver\QueryInterface */
	abstract protected function createQuery();

	/** @return Driver\TableMetadataInterface */
	abstract protected function createTableMetadata();

	/** @return callable */
	abstract protected function getExplainResult(Driver\QueryInterface $query);
}
