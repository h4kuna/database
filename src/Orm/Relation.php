<?php

namespace h4kuna\Database\Orm;

use h4kuna\Database\SqlBuilder;

class Relation
{

	const
		ONE_TO_ONE = 1,
		ONE_TO_MANY = 2;

	/** @var SqlBuilder\Table */
	private $from;

	/** @var string */
	private $fromColumn;

	/** @var string */
	private $relation = '';

	/** @var SqlBuilder\Table */
	private $to;

	/** @var string */
	private $toColumn;

	/**
	 * @param SqlBuilder\Table $table
	 * @param string $column
	 * @return self
	 */
	public function one(SqlBuilder\Table $table, $column = 'id')
	{
		$this->from = $table;
		return $this->setFromColumn($column);
	}

	public function many(SqlBuilder\Table $table, $column = '%table%_id')
	{

	}

	public function toOne(SqlBuilder\Table $table, $column = NULL)
	{
		$this->to = $table;
		if ($column === NULL) {
			$column = $table->getTable() . '_id';
		}
		$this->toColumn = $column;
		$this->relation = self::ONE_TO_ONE;
		return $this;
	}

	public function toMany(SqlBuilder\Table $table, $column = NULL)
	{

	}

	public function setMany(SqlBuilder\Table $table, $column = NULL)
	{
		$this->setNextOne($table, $column);
		$this->relation = self::ONE_TO_MANY;
		return $this;
	}

	private function setFromColumn($column)
	{
		if ($column === NULL) {
			$column = 'id';
		}
		$this->fromColumn = $column;
		return $this;
	}

	private function setNextOne(SqlBuilder\Table $table, $column)
	{

	}

}
