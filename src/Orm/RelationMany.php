<?php

namespace h4kuna\Database\Orm;

use h4kuna\Database\SqlBuilder;

class RelationMany
{

	/** @var Relation */
	private $relation1;

	/** @var Relation */
	private $relation2;

	public function __construct(Relation $relation1, Relation $relation2)
	{
		$this->relation1 = $relation1;
		$this->relation2 = $relation2;
	}

}
