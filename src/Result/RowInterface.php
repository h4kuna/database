<?php

namespace h4kuna\Database\Result;

interface RowInterface
{

	function setColumn($column, $value);

	/** @return mixed first value of result */
	function getFirstValue();
}
