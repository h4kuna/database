<?php

namespace h4kuna\Database\Result;

use h4kuna\Database\Storage\Driver,
	h4kuna\Database\Reflection;

class RowCollection implements \IteratorAggregate, \Countable
{

	/** @var Driver\SourceInterface */
	private $source;

	/** @var ResultConfigInterface */
	private $reflection;

	public function __construct(Driver\SourceInterface $source, Reflection\ReflectionInterface $reflection)
	{
		$this->reflection = $reflection;
		$this->source = $source;
	}

	public function fetchAll($key = NULL, $value = NULL)
	{
		if ($key === NULL && $value === NULL) {
			return $this->source->getRows();
		} elseif ($key === NULL) {
			return $this->fetchValue($value);
		} elseif ($value === NULL) {
			return $this->fetchAssoc($key);
		}
		return $this->fetchPairs($key, $value);
	}

	public function fetchValue($value)
	{
		$out = [];
		foreach ($this as $row) {
			$out[] = $row->{$value};
		}
		return $out;
	}

	public function fetchAssoc($key)
	{
		$out = [];
		foreach ($this as $row) {
			$out[$row->{$key}] = $row;
		}
		return $out;
	}

	public function fetchPairs($key, $value)
	{
		if ($key === NULL) {
			return $this->fetchValue($value);
		} elseif ($value === NULL) {
			return $this->fetchAssoc($key);
		}
		$out = [];
		foreach ($this as $row) {
			$out[$row->{$key}] = $row->{$value};
		}
		return $out;
	}

	public function fetchSingle()
	{
		$row = $this->fetch();
		if ($row instanceof RowInterface) {
			return $row->getFirstValue();
		}
		return NULL;
	}

	public function fetch()
	{
		foreach ($this as $row) {
			return $row;
		}
		return NULL;
	}

	public function getIterator()
	{
		return $this->source;
	}

	public function count()
	{
		return $this->source->count();
	}

	// pg_affected_rows
	// pg_convert
	// pg_fetch_all_columns
	// pg_fetch_all
	// pg_fetch_array
	// pg_fetch_assoc
	// pg_fetch_object
	// pg_fetch_result
	// pg_fetch_row
	// pg_field_is_null
	// pg_field_name
	// pg_field_num
	// pg_field_size
	// pg_field_table
	// pg_field_type_oid
	// pg_field_type
	// pg_last_oid
	// pg_result_error_field
	// pg_result_error
	// pg_result_seek
	// pg_result_status
}
