<?php

namespace h4kuna\Database\Result;

use h4kuna\Database;

class Row implements RowInterface
{

	/** @var array */
	private $data;

	public function __get($name)
	{
		if (isset($this->data[$name])) {
			return $this->data[$name];
		}
		throw new Database\PropertyDoesNotExistsException($name);
	}

	public function setColumn($column, $value)
	{
		$this->data[$column] = $value;
	}

	public function getFirstValue()
	{
		return reset($this->data);
	}

}
