<?php

namespace h4kuna\Database\Result;

use h4kuna\Database\Reflection,
	h4kuna\Database\Storage\Driver;

class RowCollectionFactory
{

	/** @var Reflection\ReflectionInterface */
	private $reflection;

	/** @var Driver\GrammarInterface */
	private $grammar;

	public function __construct(Reflection\ReflectionInterface $reflection, Driver\GrammarInterface $grammar)
	{
		$this->reflection = $reflection;
		$this->grammar = $grammar;
	}

	public function create(Driver\SourceInterface $source)
	{
		$source->setGrammar($this->grammar);
		$source->setReflection($this->reflection);
		return new RowCollection($source, $this->reflection);
	}

}
