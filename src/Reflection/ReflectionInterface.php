<?php

namespace h4kuna\Database\Reflection;

use h4kuna\Database\Result;

interface ReflectionInterface
{

	/** @return Result\RowInterface */
	function createRow(array $row, array $columnTypes);
}
