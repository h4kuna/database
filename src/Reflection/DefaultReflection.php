<?php

namespace h4kuna\Database\Reflection;

use h4kuna\DataType\Basic,
	h4kuna\Database\Storage\Driver;

class DefaultReflection implements ReflectionInterface
{

	/** @var string[] */
	private static $columnTemp = [];

	/** @var Driver\GrammarInterface */
	private $grammar;

	public function __construct(Driver\GrammarInterface $grammar)
	{
		$this->grammar = $grammar;
	}

	public function createRow(array $row, array $columnTypes)
	{
		$rowObject = new \h4kuna\Database\Result\Row();
		foreach ($row as $column => $value) {
			$rowObject->setColumn(self::normalizeColumn($column), $this->grammar->normalizeValue($value, $columnTypes[$column]));
		}
		return $rowObject;
	}

	private static function normalizeColumn($column)
	{
		if (isset(self::$columnTemp[$column])) {
			return self::$columnTemp[$column];
		}

		return self::$columnTemp[$column] = Basic\Strings::toCamel($column);
	}

}
