<?php

namespace h4kuna\Database;

abstract class DatabaseException extends \Exception {}

// Builder
abstract class SqlBuilderException extends DatabaseException {}

class InvalidArrayException extends SqlBuilderException {}

class DuplicityTableAliasException extends SqlBuilderException {}

class TableNotFoundException extends SqlBuilderException {}

class DangerousQueryException extends SqlBuilderException {}

class InvalidArgumentException extends SqlBuilderException {}

// Statement
abstract class RuntimeException extends DatabaseException {}

class NotSupportedException extends RuntimeException {}

class MethodDoesNotExistsException extends RuntimeException {}

class InvalidStateException extends RuntimeException {}

// Driver
abstract class DriverException extends DatabaseException {}

class ConnectionFailedException extends DriverException {}

class LowPhpVersionException extends DriverException {}

class LowDatabaseVersionException extends DriverException {}

class TraceConnectionFaildException extends DriverException {}

// Query
class QueryException extends DatabaseException {}

class PropertyDoesNotExistsException extends QueryException {}

class DumplicityColumnNameException extends QueryException {}

// ConstraintViolationException
abstract class ConstraintViolationException extends DatabaseException {}

class TableAlreadyExistsException extends ConstraintViolationException {}

class NotNullConstraintViolationException extends ConstraintViolationException {}

class ForeignKeyConstraintViolationException extends ConstraintViolationException {}

abstract class ConstraintInfoException extends DatabaseException
{
	private $values;
	private $columns;

	public function init(array $columns, array $values)
	{
		$this->values = $values;
		$this->columns = $columns;
	}

	public function getValues()
	{
		return $this->values;
	}

	public function getColumns()
	{
		return $this->columns;
	}

	public function getUnique()
	{
		return array_combine($this->columns, $this->values);
	}
}

class UniqueConstraintViolationException extends ConstraintInfoException {}

class ForeingKeyContraintViolationException extends ConstraintInfoException {}